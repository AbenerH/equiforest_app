package com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_FContenedor_Helper;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class Contenedor_TablaDetalle extends AppCompatActivity implements ITableActivity {

    String facturaNo;

    TableView<String[]> tableView;
    Table_FContenedor_Helper tableFContenedorHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenedor__tabla_detalle);

        showData();
    }

    @Override
    public void showData() {

        tableFContenedorHelper = new Table_FContenedor_Helper(this);
        tableView = (TableView<String[]>) findViewById(R.id.tableView_FContenedores_detalle);
        tableView.setColumnCount(4);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableFContenedorHelper.getHeaders(Headers.DETAIL_HEADER)));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableFContenedorHelper.getContenedores()));

    }
}
