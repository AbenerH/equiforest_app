package com.equiforestapp.guidu.equiforestapp.Activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.Contenedor_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_FContenedor_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;
import com.equiforestapp.guidu.equiforestapp.R;

import java.util.Calendar;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class CrearContenedor extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, ITableActivity {

    private static String id;
    private static Boolean setted = false;
    private static Boolean modify = false;


    EditText nContenedor, cabezal, chofer, sello, fechaSalida, maxcargo;

    Button guardar, modificar, eliminar, limpiar, ver;

    private TableView<String[]> tableView;
    private Table_FContenedor_Helper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_contenedor);

        initialize();
        showData();

        changeState();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setted = false;
        modify = false;
        id = "";
    }

    private void initialize() {

        nContenedor = findViewById(R.id.ContenedorNumero_edit);
        cabezal = findViewById(R.id.ContenedorCabezal_edit);
        chofer = findViewById(R.id.ContenedorChofer_edit);
        sello = findViewById(R.id.ContenedorSello_edit);
        fechaSalida = findViewById(R.id.ContenedorFechaSalida_edit);
        maxcargo = findViewById(R.id.ContenedorMAXWeight_edit);

        guardar = findViewById(R.id.guardarContenedor_button);
        modificar = findViewById(R.id.modificarContenedor_button);
        ver = findViewById(R.id.verContenedores_button);
        eliminar = findViewById(R.id.eliminarContenedor_button);
        limpiar = findViewById(R.id.limpiarContenedor_button);

        fechaSalida.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus)
                    showDatePickerDialog();
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!checkFields()) {   //Verify that fields aren't empty , if not...build a new Object

                    Contenedor contenedor = new Contenedor(); //Container Construction

                    contenedor.setContenedorId(nContenedor.getText().toString());
                    contenedor.setCabezal(cabezal.getText().toString());
                    contenedor.setSello(sello.getText().toString());
                    contenedor.setFechaSalida(fechaSalida.getText().toString());
                    contenedor.setChofer(chofer.getText().toString());
                    contenedor.setMaxCargoWgt(Double.parseDouble(maxcargo.getText().toString()));

                    if (setted) {

                        Intent intent = new Intent(CrearContenedor.this, AddPackingList.class);
                        intent.putExtra("Contenedor", contenedor);
                        startActivity(intent);

                    } else {

                        //Check if #Container doesn't exists already
                        if (new DBAdapter(CrearContenedor.this).checkContenedor(contenedor.getContenedorId())) {

                            Toast.makeText(CrearContenedor.this, "Ya existe un contenedor con este numero", Toast.LENGTH_SHORT).show();
                        } else {

                            //Check if seal doesn't exists already
                            if (new DBAdapter(CrearContenedor.this).checkContenedorSeal(contenedor.getSello())) {

                                Toast.makeText(CrearContenedor.this, "Ya existe un contenedor con este sello", Toast.LENGTH_SHORT).show();

                            } else {

                                //If everything's okay...Try to Save this container
                                if (new DBAdapter(CrearContenedor.this).guardarContenedor(contenedor)) {

                                    Log.d("Debugging", "Contenedor almacenado: " + contenedor.toString());
                                    Toast.makeText(CrearContenedor.this, "Contenedor creado exitosamente!", Toast.LENGTH_SHORT).show();
                                    cleanFields();
                                    refreshTable();
                                } else {
                                    Log.d("Debugging", "Error al crear contenedor: ");
                                    Toast.makeText(CrearContenedor.this, "Error al crear el contenedor!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                } else {
                    Toast.makeText(CrearContenedor.this, "No pueden haber campos vacios", Toast.LENGTH_SHORT).show();
                }
            }
        });

        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (modify) {

                    if (!checkFields()){

                        Contenedor contenedor = new Contenedor();
                        contenedor.setContenedorId(id);
                        contenedor.setCabezal(cabezal.getText().toString());
                        contenedor.setChofer(chofer.getText().toString());
                        contenedor.setSello(sello.getText().toString());
                        contenedor.setFechaSalida(fechaSalida.getText().toString());
                        contenedor.setMaxCargoWgt(Double.parseDouble(maxcargo.getText().toString()));

                        if (new DBAdapter(CrearContenedor.this).updateContenedor(contenedor)) {

                            Toast.makeText(CrearContenedor.this, "Contenedor Modificado exitosamente", Toast.LENGTH_SHORT).show();
                            Log.d("Debugging", "Contenedor modificado exitosamente");

                        } else {
                            Toast.makeText(CrearContenedor.this, "Error al modificar este contenedor", Toast.LENGTH_SHORT).show();
                            Log.d("Debugging", "Contenedor no se pudo modificar");

                        }

                        Log.d("Debugging", "ID: " + contenedor.getId());

                        modify = false;
                        setted = true;
                        refreshTable();
                        changeState();

                    } else{

                        Toast.makeText(CrearContenedor.this, "Hay un campo sin valor que no puede quedar vacio!", Toast.LENGTH_SHORT).show();
                        
                    }
                    
                } else {

                    modify = true;
                    setted = false;
                    changeState();

                }
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alerta = new AlertDialog.Builder(CrearContenedor.this);
                alerta.setMessage("¿Desea borrar este contenedor?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            setted = false;

                            if (new DBAdapter(CrearContenedor.this).eliminarContenedor(id)) {

                                Toast.makeText(CrearContenedor.this, "Registro borrado exitosamente", Toast.LENGTH_SHORT).show();
                                cleanFields();
                                setted = false;
                                changeState();
                                refreshTable();

                            } else {
                                Toast.makeText(CrearContenedor.this, "Error al eliminar registro", Toast.LENGTH_SHORT).show();
                            }

                            dialogInterface.dismiss();
                            }

                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });

                AlertDialog myAlert = alerta.create();
                myAlert.setTitle("Borrar Contenedor");
                myAlert.show();
            }
        });

        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cleanFields();
                setted = false;
                modify = false;
                changeState();
            }
        });

        ver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CrearContenedor.this, Contenedor_TablaDetalle.class);
                startActivity(intent);
            }
        });


    }

    private void showDatePickerDialog() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    private void refreshTable() {
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getContenedores()));
    }

    private boolean checkFields() {

        //All fields required
        return nContenedor.getText().toString().equals("") ||  maxcargo.getText().toString().equals("");

        //except Cabezal, Sello, Chofer, FechaSalida

    }

    private void cleanFields() {

        nContenedor.setText("");
        cabezal.setText("");
        chofer.setText("");
        sello.setText("");
        fechaSalida.setText("");
        maxcargo.setText("");

    }

    @Override
    public void showData() {

        tableHelper = new Table_FContenedor_Helper(this);
        tableView = (TableView<String[]>) findViewById(R.id.Contenedor_tableView);
        tableView.setColumnCount(3);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders(Headers.SIMPLE_HEADER)));
        refreshTable();

        tableView.addDataClickListener(new TableDataClickListener<String[]>() {
            @Override
            public void onDataClicked(int rowIndex, String[] clickedData) {

                nContenedor.setText(((String[]) clickedData)[0]);
                cabezal.setText(((String[]) clickedData)[1]);
                chofer.setText(((String[]) clickedData)[5]);
                sello.setText(((String[]) clickedData)[2]);
                fechaSalida.setText(((String[]) clickedData)[4]);
                maxcargo.setText(((String[]) clickedData)[3]);
                id = (((String[]) clickedData)[0]);

                setted = true;
                modify = false;
                changeState();

            }
        });
    }

    private void changeState() {

        if (setted) {

            guardar.setText("P.L");
            modificar.setText("Modificar");
            lockFields();

        } else {

            if(modify){

                guardar.setEnabled(false);
                modificar.setText("Guardar");
                nContenedor.setEnabled(false);
                sello.setEnabled(false);

            }else {
                id = "";
                guardar.setText("Guardar");

            }

            dislockFields();

        }

    }

    private void lockFields() {

        nContenedor.setEnabled(false);
        cabezal.setEnabled(false);
        chofer.setEnabled(false);
        sello.setEnabled(false);
        fechaSalida.setEnabled(false);
        maxcargo.setEnabled(false);
        eliminar.setEnabled(true);
        modificar.setEnabled(true);
        guardar.setEnabled(true);

    }

    private void dislockFields() {

        cabezal.setEnabled(true);
        chofer.setEnabled(true);
        fechaSalida.setEnabled(true);
        maxcargo.setEnabled(true);
        eliminar.setEnabled(false);

        if (!modify){
            modificar.setEnabled(false);
            guardar.setEnabled(true);
            nContenedor.setEnabled(true);
            sello.setEnabled(true);

        }

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        month = month + 1;

        String date = month + "/" + day + "/" + year;

        fechaSalida.setText(date);

    }

    private int getArrayValue(String[] array, String toFind) {

        int position = 0;

        for (int i = 0; i < array.length; i++) {

            if (array[i].equals(toFind)) {

                position = i;
            }
        }

        return position;
    }
}
