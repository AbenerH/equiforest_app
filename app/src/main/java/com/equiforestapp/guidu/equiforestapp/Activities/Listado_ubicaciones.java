package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Ubicaciones_Helper;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Viajes_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Ubicaciones;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class Listado_ubicaciones extends AppCompatActivity implements ITableActivity, AdapterView.OnItemSelectedListener {

    EditText patio;

    Button add;

    private TableView<String[]> tableView;
    private Table_Ubicaciones_Helper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_acopios);

        initialize();
        showData();

    }

     @Override
     public void showData() {

        tableHelper = new Table_Ubicaciones_Helper(this);
        tableView = (TableView<String[]>) findViewById(R.id.patio_tableView);
        tableView.setColumnCount(2);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders()));

         tableView.addDataLongClickListener(new longClickListenerHandler());

        refreshTable();

    }

    private void refreshTable() {

        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getUbicaciones()));
    }

    private void initialize() {

        patio = findViewById(R.id.patio_nombre_edit);
        add = findViewById(R.id.patio_addPatio);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!checkFields()){

                    Ubicaciones ubicacion = new Ubicaciones();
                    ubicacion.setNombre(patio.getText().toString());

                    if (new DBAdapter(Listado_ubicaciones.this).guardarUbicacion(ubicacion)){

                        Toast.makeText(Listado_ubicaciones.this, "Datos insertados correctamente", Toast.LENGTH_SHORT).show();
                        refreshTable();
                        patio.setText("");

                    } else {
                        Toast.makeText(Listado_ubicaciones.this, "Error al guardar", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(Listado_ubicaciones.this, "Debe asignar un nombre primero a la ubicacion de patio", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean checkFields() {

        return patio.getText().toString().equals("");

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        refreshTable();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class longClickListenerHandler implements de.codecrafters.tableview.listeners.TableDataLongClickListener<String[]> {

        @Override
        public boolean onDataLongClicked(int rowIndex, String[] clickedData) {
            final int id =  Integer.parseInt(((String[]) clickedData)[0]);

            AlertDialog.Builder alerta = new AlertDialog.Builder(Listado_ubicaciones.this);
            alerta.setMessage("¿Desea borrar este registro?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            if (new DBAdapter(Listado_ubicaciones.this).eliminarUbicacion(id)) {

                                Toast.makeText(Listado_ubicaciones.this, "Registro Borrado", Toast.LENGTH_SHORT).show();
                                refreshTable();

                            } else {
                                Toast.makeText(Listado_ubicaciones.this, "El registro no se pudo borrar", Toast.LENGTH_SHORT).show();
                            }

                            dialogInterface.dismiss();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Borrar Ubicacion");
            myAlert.show();

            return true;
        }

    }
}
