package com.equiforestapp.guidu.equiforestapp.Interfaces;

import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;
import com.equiforestapp.guidu.equiforestapp.Modelo.Rodal;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.Modelo.Viaje;

import java.util.ArrayList;


import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IMyAPI {


    //Cosechas

    //Get
    @Headers({"Content-Type: application/json"})
    @GET("api/Cosechas/Rodales")
    Single<ArrayList<Rodal>> getRodales();

    //Post
    @Headers({"Content-Type: application/json"})
    @POST("api/Cosechas/Viajes")
    Observable<ArrayList<Viaje>> addViajes(@Body ArrayList<Viaje> lista);

    @Headers({"Content-Type: application/json"})
    @POST("api/Cosechas/Trozas")
    Observable<ArrayList<Troza>> addTrozas(@Body ArrayList<Troza> lista);
//    Observable<ArrayList<Troza>> addTrozas(@Body ArrayList<Troza> lista);

    //Comercializacion

    @Headers({"Content-Type: application/json"})
    @POST("api/Comercializacion/Contenedores")
//    Call<ArrayList<Contenedor>> addContenedores(@Body ArrayList<Contenedor> lista);
    Observable<ArrayList<Contenedor>> addContenedores(@Body ArrayList<Contenedor> lista);

    @Headers({"Content-Type: application/json"})
    @POST("api/Comercializacion/DetalleContenedores")
//    Call<ArrayList<DetalleContenedor>> addDetalleContenedor(@Body ArrayList<DetalleContenedor> lista);
    Observable<ArrayList<DetalleContenedor>> addDetalleContenedor(@Body ArrayList<DetalleContenedor> lista);

    @Headers({"Content-Type: application/json"})
    @POST("api/Comercializacion/PackingList")
//    Call<ArrayList<PackingList>> addPackingLists(@Body ArrayList<PackingList> lista);
    Observable<ArrayList<PackingList>> addPackingLists(@Body ArrayList<PackingList> lista);

}
