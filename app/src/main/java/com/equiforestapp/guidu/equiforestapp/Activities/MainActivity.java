package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.API_Process_Cosechas;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences preferences = getSharedPreferences("preferences", MODE_PRIVATE );
        boolean firstTimeEval = preferences.getBoolean("firstTime", true);

        if (firstTimeEval){
            checkRodal();

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("firstTime", false);
            editor.apply();
        }

/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void checkRodal(){

        showAlert(1,"Descargar Rodales", "Desea inmediatamente descargar todos los rodales?");

    }

    private void showAlert(int option, String title, String message) {

        android.support.v7.app.AlertDialog.Builder alerta = new android.support.v7.app.AlertDialog.Builder(this);
        alerta.setTitle(title);
        alerta.setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Si", (dialogInterface, i) -> {

                    switch (option)
                    {
                        case 1:
                            API_Process_Cosechas process_cosechas = new API_Process_Cosechas(this);
                            process_cosechas.StartDownload();
                            break;
                        case 2:
                            
                            if(new DBAdapter(this).restartAllFromDb())
                            {
                                Toast.makeText(this, "Todos los valores han sido reiniciados", Toast.LENGTH_SHORT).show();
                            }else
                            {
                                Toast.makeText(this, "Error al reiniciar estados", Toast.LENGTH_SHORT).show();
                            }
                                
                            break;
                        case 3:

                            alertDeletefromDb();
                            break;

                        case 4:

                            if(new DBAdapter(this).deleteAllFromDb())
                            {
                                Toast.makeText(this, "Todos los registros han sido eliminados", Toast.LENGTH_SHORT).show();
                            }else
                            {
                                Toast.makeText(this, "Error al eliminar los registros", Toast.LENGTH_SHORT).show();
                            }

                            break;
                    }
                }).setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

        alerta.show();

    }

    private void alertDeletefromDb() {

        showAlert(4, "Eliminacion permanente de todos los datos", "Esta seguro de querer eliminar TODOS los registros de la base de datos? OJO: Una vez eliminados no se podran recuperar!");

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id)
        {
            case R.id.descargar_rodales:
                API_Process_Cosechas process_cosechas = new API_Process_Cosechas(MainActivity.this);
                process_cosechas.StartDownload();
            break;

            case R.id.reiniciar_estado:

                showAlert(2, "Reiniciar todos los estados", "Desea reiniciar todos los valores de estado para asi poder ser subidos de nuevo?");

            break;
            case R.id.borrar_datos:
                showAlert(3, "Borrar todos los registros", "Esta seguro de querer borrar todos los registros? Una vez borrados no se podran recuperar!");
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        switch (id) {
            case R.id.nav_cosechas:
                Intent cosecha = new Intent(this, Cosecha_Menu.class);
                startActivity(cosecha);
            break;
            case R.id.nav_comercializacion:
                Intent comerc = new Intent(this, Comercializacion_Menu.class);
                startActivity(comerc);
                break;

            case R.id.nav_ubicacion:
                Intent ubicacion = new Intent(this, Listado_ubicaciones.class);
                startActivity(ubicacion);
                break;

            case R.id.nav_rodal:
                Intent rodal = new Intent(this, CrearRodales.class);
                startActivity(rodal);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}

