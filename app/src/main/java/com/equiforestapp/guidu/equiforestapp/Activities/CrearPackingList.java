package com.equiforestapp.guidu.equiforestapp.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.PL_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_PackingList_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;
import com.equiforestapp.guidu.equiforestapp.Modelo.Ubicaciones;
import com.equiforestapp.guidu.equiforestapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class CrearPackingList extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, ITableActivity, AdapterView.OnItemSelectedListener {

    private static Boolean setted = false;
    private static String Id = "";
    private static String final_date = "";

    private ArrayList<String> clases;

    private ArrayList<String>ubicaciones;

    TextView fecha;

    EditText consecutivo, descuentoCrc, descuentoLong;

    Spinner ubicacion_spinner, clase_spinner;

    Button guardar, eliminar, limpiar, verTabla;

    private TableView<String[]> tableView;
    private Table_PackingList_Helper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_packing_list);

        initialize();
        showData();

        changeState();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setted = false;
        Id = "";

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_agregar_viaje);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("packingId", Id);
        outState.putBoolean("setted", setted);

        outState.putString("fecha", fecha.getText().toString());

        if (ubicacion_spinner.getSelectedItem() != null){
            outState.putInt("ubicacion", getArrayValue(ubicaciones, ubicacion_spinner.getSelectedItem().toString()));
        }

        outState.putString("consecutivo", consecutivo.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        fecha.setText(savedInstanceState.getString("fecha"));
        consecutivo.setText(savedInstanceState.getString("consecutivo"));

        Id = savedInstanceState.getString("packingId");
        setted = savedInstanceState.getBoolean("setted");
    }

    @Override
    public void showData() {

        tableHelper = new Table_PackingList_Helper(this);
        tableView = findViewById(R.id.packing_tableView);
        TableColumnWeightModel columnModel = new TableColumnWeightModel(4);
        columnModel.setColumnWeight(0, 2);
        columnModel.setColumnWeight(1, 1);
        columnModel.setColumnWeight(2, 2);
        columnModel.setColumnWeight(3, 1);
        tableView.setColumnModel(columnModel);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders(Headers.SIMPLE_HEADER)));
        refreshTable();

        tableView.addDataClickListener((rowIndex, clickedData) -> {

            Id = (clickedData[7]);
            consecutivo.setText(clickedData[1]);
            fecha.setText(clickedData[2]);
            ubicacion_spinner.setSelection(getArrayValue(ubicaciones, clickedData[0]));
            descuentoCrc.setText(clickedData[4]);
            descuentoLong.setText(clickedData[5]);
            clase_spinner.setSelection(getArrayValue(clases, clickedData[3]));

            setted = true;
            changeState();

        });
    }

    private void initialize() {

        clases = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.clasif_trozas_array)));

        fecha = findViewById(R.id.packing_fecha_text);
        consecutivo = findViewById(R.id.packing_consecutivo_edit);
        descuentoCrc = findViewById(R.id.packing_descuentoCrc_edit);
        descuentoLong = findViewById(R.id.packing_descuentoLong_edit);
        clase_spinner = findViewById(R.id.packing_clase_spinner);
        ubicacion_spinner = findViewById(R.id.packing_ubicacion_spinner);

        guardar = findViewById(R.id.packing_guardar_button);
        eliminar = findViewById(R.id.packing_eliminar_button);
        limpiar = findViewById(R.id.packing_limpiar_button);
        verTabla = findViewById(R.id.verTablaPL_button);

        getAcopioData();

/*        final ArrayAdapter<String> acopioAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, ubicaciones);
        ubicacion_spinner.setAdapter(acopioAdapter);*/

        ArrayAdapter<CharSequence> clasesAdapter = ArrayAdapter.createFromResource(this, R.array.clasif_trozas_array, R.layout.support_simple_spinner_dropdown_item);
        clasesAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        clase_spinner.setAdapter(clasesAdapter);

        fecha.setOnClickListener(view -> showDatePickerDialog());

        guardar.setOnClickListener(view -> {

            if (!checkFields()) { //Check for empty fields

                PackingList packingList = new PackingList();

                int nConsecutivo = Integer.parseInt(consecutivo.getText().toString());
                String clasificacion = clase_spinner.getSelectedItem().toString();
                String ubicacion = ubicacion_spinner.getSelectedItem().toString();
                String ubicacionId = ubicacion.replaceAll("\\s+", "");
                String date = fecha.getText().toString();

                packingList.setFecha( date);
                packingList.setConsecutivo(nConsecutivo);
                packingList.setClase(clasificacion);
                packingList.setUbicacion(ubicacion);
                packingList.setDescuentoCrc(Double.parseDouble(descuentoCrc.getText().toString()));
                packingList.setDescuentoLong(Double.parseDouble(descuentoLong.getText().toString()));
                final_date = getFormatDate(date);
                packingList.setPackingListId(ubicacionId + final_date + nConsecutivo + clasificacion);

                Log.d("Debugging", packingList.toString());

                if (setted) {

                    String fecha = packingList.getFecha();
                    String consecutivo = String.valueOf(packingList.getConsecutivo());
                    String clase = packingList.getClase();

                    String packingId = Id;

                    Log.d("Debugging", "Objeto a pasar: " + fecha + "-" + consecutivo + "-" + clase + " = " + packingId);

                    Intent intent = new Intent(CrearPackingList.this, FacturaDetalleContenedor.class);
                    intent.putExtra("packingId", packingId);
                    intent.putExtra("fecha", fecha);
                    intent.putExtra("consecutivo", consecutivo);
                    intent.putExtra("clase", clase);
                    startActivity(intent);

                } else {

                    if (new DBAdapter(CrearPackingList.this).checkPackingList(packingList.getPackingListId())) {

                        Toast.makeText(CrearPackingList.this, "Ya existe un registro con este identificador", Toast.LENGTH_SHORT).show();

                    } else {

                        if (new DBAdapter(CrearPackingList.this).guardarPackingList(packingList)) {

                            cleanFields();
                            Toast.makeText(CrearPackingList.this, "Datos insertados correctamente", Toast.LENGTH_SHORT).show();
                            refreshTable();

                        } else {

                            Toast.makeText(CrearPackingList.this, "Error al guardar", Toast.LENGTH_SHORT).show();

                        }
                    }
                }

            } else {

                Toast.makeText(CrearPackingList.this, "No pueden haber campos vacios", Toast.LENGTH_SHORT).show();
            }
        });

        eliminar.setOnClickListener(view -> {

            AlertDialog.Builder alerta = new AlertDialog.Builder(CrearPackingList.this);
            alerta.setMessage("¿Desea borrar este PackingList")
                    .setCancelable(false)
                    .setPositiveButton("Si", (dialogInterface, i) -> {

                        String packingid = Id;

                        setted = false;

                        if (new DBAdapter(CrearPackingList.this).eliminarPackingList(packingid)) {

                            Toast.makeText(CrearPackingList.this, "Registro borrado exitosamente", Toast.LENGTH_SHORT).show();
                            cleanAllFields();
                            changeState();
                            refreshTable();

                        } else {
                            Toast.makeText(CrearPackingList.this, "Error al eliminar registro", Toast.LENGTH_SHORT).show();
                        }

                        dialogInterface.dismiss();
                    })
                    .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

            AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Borrar Packing List");
            myAlert.show();
        });

        limpiar.setOnClickListener(view -> {
            cleanFields();
            setted = false;
            changeState();

        });

        limpiar.setOnLongClickListener(view -> {
            cleanAllFields();
            setted = false;
            changeState();
            return true;
        });

        verTabla.setOnClickListener(view -> {

            Intent intent = new Intent(CrearPackingList.this, PL_TablaDetalle.class);
            startActivity(intent);

        });

    }

    private void changeState() {

        if (setted) {

            guardar.setText("Asignar");
            eliminar.setEnabled(true);
            clase_spinner.setEnabled(false);
            ubicacion_spinner.setEnabled(false);
            fecha.setEnabled(false);
            consecutivo.setEnabled(false);
            descuentoCrc.setEnabled(false);
            descuentoLong.setEnabled(false);

        } else {

            Id = "";
            guardar.setText(R.string.guardar_text);
            eliminar.setEnabled(false);
            clase_spinner.setEnabled(true);
            ubicacion_spinner.setEnabled(true);
            fecha.setEnabled(true);
            consecutivo.setEnabled(true);
            descuentoCrc.setEnabled(true);
            descuentoLong.setEnabled(true);

        }

    }

    private void refreshTable() {

        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getPackingLists()));
    }

    private void cleanFields() {

        consecutivo.setText("");
        descuentoCrc.setText("");
        descuentoLong.setText("");

    }

    private void cleanAllFields() {

        fecha.setText("");
        consecutivo.setText("");
        clase_spinner.setSelection(0);
        ubicacion_spinner.setSelection(0);
        descuentoCrc.setText("");
        descuentoLong.setText("");

    }

    private boolean checkFields() {

        return ubicacion_spinner.getSelectedItem() == null ||  fecha.getText().toString().equals("") || consecutivo.getText().toString().equals("") || descuentoCrc.getText().toString().equals("") || descuentoLong.getText().toString().equals("");

    }

    private int getArrayValue(ArrayList<String> array, String toFind) {
        int position = 0;

        for (int i = 0; i < array.size(); i++) {

            if (array.get(i).equals(toFind)) {

                position = i;
            }
        }

        return position;
    }

    private void getAcopioData(){

        ubicaciones = new ArrayList<>();

        ArrayList<Ubicaciones> rodales = new DBAdapter(CrearPackingList.this).recuperarListaUbicaciones();

        for (int i = 0; i < rodales.size() ; i++) {

            ubicaciones.add(rodales.get(i).getNombre());

        }

        final ArrayAdapter<String> acopioAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, ubicaciones);
        ubicacion_spinner.setAdapter(acopioAdapter);

    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        month = month + 1;

        String date = month + "/" + day + "/" + year;
        final_date = getFormatDate(date);

        fecha.setText(date);

    }

    private String getFormatDate(String date){

        return date.replaceAll( "[^a-zA-Z0-9 ]" , "" );

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
