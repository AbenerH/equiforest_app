package com.equiforestapp.guidu.equiforestapp.Modelo;

public class Viaje {

    private int id;
    private String nucleo;
    private String finca;
    private String rodal;
    private String fechaIngreso;
    private String ubicacion;
    private String viajeid;
    private int n_viaje;
    private int noTrozasC;


    public Viaje() {
    }

    public Viaje(int id, String nucleo, String finca, String rodal, String fechaIngreso, String ubicacion, String viajeid, int n_viaje, int noTrozasC) {
        this.id = id;
        this.nucleo = nucleo;
        this.finca = finca;
        this.rodal = rodal;
        this.fechaIngreso = fechaIngreso;
        this.ubicacion = ubicacion;
        this.viajeid = viajeid;
        this.n_viaje = n_viaje;
        this.noTrozasC = noTrozasC;
    }

    public void setId_viaje(int id_viaje) {
        this.id= id_viaje;
    }

    public int getId_viaje() {
        return id;
    }

    public String getNucleo() {
        return nucleo;
    }

    public void setNucleo(String nucleo) {
        this.nucleo = nucleo;
    }

    public String getFinca() {
        return finca;
    }

    public void setFinca(String finca) {
        this.finca = finca;
    }

    public String getRodal() {
        return rodal;
    }

    public void setRodal(String rodal) {
        this.rodal = rodal;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getN_viaje() {
        return n_viaje;
    }

    public void setN_viaje(int n_viaje) {
        this.n_viaje = n_viaje;
    }

    public String getViajeid() {
        return viajeid;
    }

    public void setViajeid(String viajeid) {
        this.viajeid = viajeid;
    }

    public int getNoTrozasC() {
        return noTrozasC;
    }

    public void setNoTrozasC(int noTrozasC) {
        this.noTrozasC = noTrozasC;
    }

    @Override
    public String toString() {
        return "Viaje{" +
                "id=" + id +
                ", nucleo='" + nucleo + '\'' +
                ", finca='" + finca + '\'' +
                ", rodal='" + rodal + '\'' +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", ubicacion='" + ubicacion + '\'' +
                ", viajeid='" + viajeid + '\'' +
                ", n_viaje=" + n_viaje +
                ", noTrozasC=" + noTrozasC +
                '}';
    }
}
