package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;

import java.util.ArrayList;

public class Table_PackingList_Helper {

    Context context;

    private String [] headers;
    private String[][] data;

    public Table_PackingList_Helper(Context context) {
        this.context = context;
    }

    public String[] getHeaders(Headers header_type) {

        switch (header_type){

            case SIMPLE_HEADER:
                headers = new String[]{"Ubicacion","#", "Fecha", "Clase"};
                break;

            case DETAIL_HEADER:
                headers = new String[]{"Ubicacion","#", "Fecha", "Clase" , "Desc. Circ", "Desc. Long", "Contenedor"};
                break;
        }

        return headers;
    }

    public String[][] getPackingLists(){

        ArrayList<PackingList> listadoPackinglist = new DBAdapter(context).recuperarListaPackingList(1);
        PackingList packingList;

        data = new String[listadoPackinglist.size()][8];

        for (int i = 0; i < listadoPackinglist.size() ; i++) {

            packingList = listadoPackinglist.get(i);

            data[i][0] = packingList.getUbicacion();
            data[i][1] = String.valueOf(packingList.getConsecutivo());
            data[i][2] = packingList.getFecha();
            data[i][3] = packingList.getClase();
            data[i][4] = String.valueOf(packingList.getDescuentoCrc());
            data[i][5] = String.valueOf(packingList.getDescuentoLong());
            data[i][6] = packingList.getContenedorId();
            data[i][7] = packingList.getPackingListId();

        }

        return data;

    }

    public String[][] getPackingListsDisponibles(){

        ArrayList<PackingList> listadoPackinglist = new DBAdapter(context).recuperarListaPackingListDisponibles();
        PackingList packingList;

        data = new String[listadoPackinglist.size()][5];

        for (int i = 0; i < listadoPackinglist.size() ; i++) {

            packingList = listadoPackinglist.get(i);

            data[i][0] = packingList.getUbicacion();
            data[i][1] = String.valueOf(packingList.getConsecutivo());
            data[i][2] = packingList.getFecha();
            data[i][3] = packingList.getClase();
            data[i][4] = packingList.getPackingListId();

        }

        return data;

    }

}
