package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.API_Process_Cosechas;
import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.Cosechas_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.R;

public class Cosecha_Menu extends AppCompatActivity {

    private Button viajes, verTrozas, volumenes, uploadData;

    int intentos;

    boolean dataFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosecha_menu);

        initialize();

    }

    private void initialize() {

        viajes = findViewById(R.id.asignarViaje_button);
        verTrozas = findViewById(R.id.verTrozas_button);
        volumenes = findViewById(R.id.volumenes_button);
        uploadData = findViewById(R.id.uploadCosechasData_button);

        intentos = 0;

        viajes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (new DBAdapter(Cosecha_Menu.this).recuperarListaRodales().size() > 0){
                    dataFound = true;
                }

                if(dataFound){
                    Intent intent = new Intent(getApplicationContext(), CrearViaje.class);
                    startActivity(intent);
                }else{

                    intentos++;

                    if (intentos>3){
                        openDialog();
                    }else{
                        Toast.makeText(Cosecha_Menu.this, "No hay datos de rodales, no se pueden registrar Viajes", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        verTrozas.setOnClickListener(view -> {
            Intent intent = new Intent(Cosecha_Menu.this, Cosechas_TablaDetalle.class);
            startActivity(intent);
        });

        volumenes.setOnClickListener(view -> {
            Intent intent = new Intent(Cosecha_Menu.this, VolumenesDiarios.class);
            startActivity(intent);
        });

        uploadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                API_Process_Cosechas api_processCosechas = new API_Process_Cosechas(Cosecha_Menu.this);
                api_processCosechas.StartUpload();

/*              Log.d("Debugging", "UPLOAD DATA SHOULD BE COMPLETED");
                updated = new DBAdapter(Cosecha_Menu.this).updateViajesEstado();
                Log.d("Debugging", "Viajes updated == " + updated);
                updated = new DBAdapter(Cosecha_Menu.this).updateTrozasEstado();
                Log.d("Debugging", "Trozas updated == " + updated);*/

            }
        });

    }

    private void openDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(Cosecha_Menu.this);
        builder.setMessage(R.string.noRodalesInfo)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

        AlertDialog myAlert = builder.create();
        myAlert.setTitle("Debe descargar Datos de Rodales");
        myAlert.show();
    }

}
