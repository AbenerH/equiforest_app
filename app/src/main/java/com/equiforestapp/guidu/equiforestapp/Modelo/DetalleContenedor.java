package com.equiforestapp.guidu.equiforestapp.Modelo;

import java.text.DecimalFormat;

public class DetalleContenedor {

    private int id;
    private String packingListId;
    private double circunferencia;
    private double longitud;

    public DetalleContenedor() {
    }

    public DetalleContenedor(int id, String packingListId, double circunferencia, double longitud) {
        this.id = id;
        this.packingListId = packingListId;
        this.circunferencia = circunferencia;
        this.longitud = longitud;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackingListId() {
        return packingListId;
    }

    public void setPackingListId(String packingListId) {
        this.packingListId = packingListId;
    }

    public double getCircunferencia() {
        return circunferencia;
    }

    public void setCircunferencia(double circunferencia) {
        this.circunferencia = circunferencia;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getDiametro(){

        return getCircunferencia()/Math.PI;
    }

    public double getSmalian(){

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        double smalian = 0;

        double result = ((((getDiametro() / 100) * (getDiametro() / 100)) / 4) * Math.PI * getLongitud());

        String converted = decimalFormat.format(result);

        smalian = Double.parseDouble(converted);

        return smalian;
    }

    @Override
    public String toString() {
        return "DetalleContenedor{" +
                "id=" + id +
                ", packingListId='" + packingListId + '\'' +
                ", circunferencia=" + circunferencia +
                ", longitud=" + longitud +
                '}';
    }
}
