package com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Viajes_Helper;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class Viajes_TablaDetalle extends AppCompatActivity implements ITableActivity {

    TableView<String[]> tableView;
    Table_Viajes_Helper tableViajesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viajes__tabla_detalle);

        showData();
    }

    @Override
    public void showData() {

        tableViajesHelper = new Table_Viajes_Helper(this);
        tableView = (TableView<String[]>) findViewById(R.id.tableView_viajes_detalle);
        tableView.setColumnCount(8);
        tableView.setHeaderBackgroundColor(Color.parseColor("#a6a6a6"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableViajesHelper.getHeaders(Headers.SIMPLE_HEADER)));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableViajesHelper.getViajes()));

    }
}
