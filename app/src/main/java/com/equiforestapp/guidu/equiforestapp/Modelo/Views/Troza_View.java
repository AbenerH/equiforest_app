package com.equiforestapp.guidu.equiforestapp.Modelo.Views;

import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;

public class Troza_View extends Troza {

    private String viajeid;
    private String finca;
    private String fechaIngreso;

    private String rodal;

    public String getViajeid() {
        return viajeid;
    }

    public void setViajeid(String viajeid) {
        this.viajeid = viajeid;
    }

    public String getFinca() {
        return finca;
    }

    public void setFinca(String finca) {
        this.finca = finca;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getRodal() {
        return rodal;
    }

    public void setRodal(String rodal) {
        this.rodal = rodal;
    }

}
