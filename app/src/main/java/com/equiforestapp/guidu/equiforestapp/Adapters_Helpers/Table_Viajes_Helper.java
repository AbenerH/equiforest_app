package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;
import android.util.Log;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.Viaje;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Table_Viajes_Helper {

    Context context;

    private String [] headers;
    // ={"ID", "Nucleo", "Finca", "Rodal", "Fecha_Ingreso", "Patio_Acopio", "Trozas C", "Viaje_No" };
    private String[][] data;

    public Table_Viajes_Helper(Context context){
        this.context = context;
    }

    public String[] getHeaders(Headers header) {

        switch (header){

            case SIMPLE_HEADER: headers = new String[]{"ID", "Nucleo", "Finca", "Rodal", "Fecha_Ingreso", "Patio_Acopio", "Trozas C", "Viaje_No" };;
                break;

            case DETAIL_HEADER: headers = new String[]{"Fecha", "Viajes", "Volumen"};
                break;
        }

        return headers;
    }

    public String[][] getViajes(){

        ArrayList<Viaje> listadoViajes= new DBAdapter(context).recuperarListaViajes(1);
        Viaje viaje;

        data = new String[listadoViajes.size()][8];

        for (int i = 0; i < listadoViajes.size() ; i++) {

            viaje = listadoViajes.get(i);

            data[i][0] = String.valueOf(viaje.getId_viaje());
            data[i][1] = viaje.getNucleo();
            data[i][2] = viaje.getFinca();
            data[i][3] = viaje.getRodal();
            data[i][4] = viaje.getFechaIngreso();
            data[i][5] = viaje.getUbicacion();
            data[i][6] = String.valueOf(viaje.getNoTrozasC());
            data[i][7] = String.valueOf(viaje.getN_viaje());
        }

        return data;
    }

    public String[][] getViajesxFecha(){

        ArrayList<String> fechas =  new DBAdapter(context).getFechas();
        double volumen;
        int nViajes;

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        data = new String[fechas.size()][3];

        for (int i = 0; i < fechas.size(); i++) {

            volumen = new DBAdapter(context).getVolumenesxDia(fechas.get(i));
            nViajes = new DBAdapter(context).getViajesxDia(fechas.get(i)).size();

            data[i][0] = fechas.get(i);
            data[i][1] = String.valueOf(nViajes);
            data[i][2] = String.valueOf(decimalFormat.format(volumen));

        }

        return data;
    }

}
