package com.equiforestapp.guidu.equiforestapp.Modelo;

public class PackingList {

    private int id;
    private String fecha;
    private int consecutivo;
    private String clase;
    private String packingListId;
    private String contenedorId;
    private String ubicacion;
    private double descuentoCrc;
    private double descuentoLong;

    public PackingList() {
    }

    public PackingList(int id, String fecha, int consecutivo, String clase, String packingListId, String contenedorId, String ubicacion, double descuentoCrc, double descuentoLong) {
        this.id = id;
        this.fecha = fecha;
        this.consecutivo = consecutivo;
        this.clase = clase;
        this.packingListId = packingListId;
        this.contenedorId = contenedorId;
        this.ubicacion = ubicacion;
        this.descuentoCrc = descuentoCrc;
        this.descuentoLong = descuentoLong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(int consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getPackingListId() {
        return packingListId;
    }

    public void setPackingListId(String packingListId) {
        this.packingListId = packingListId;
    }

    public String getContenedorId() {
        return contenedorId;
    }

    public void setContenedorId(String contenedorId) {
        this.contenedorId = contenedorId;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public double getDescuentoCrc() {
        return descuentoCrc;
    }

    public void setDescuentoCrc(double descuentoCrc) {
        this.descuentoCrc = descuentoCrc;
    }

    public double getDescuentoLong() {
        return descuentoLong;
    }

    public void setDescuentoLong(double descuentoLong) {
        this.descuentoLong = descuentoLong;
    }

    @Override
    public String toString() {
        return "PackingList{" +
                "id=" + id +
                ", fecha='" + fecha + '\'' +
                ", consecutivo=" + consecutivo +
                ", clase='" + clase + '\'' +
                ", packingListId='" + packingListId + '\'' +
                ", contenedorId='" + contenedorId + '\'' +
                ", ubicacion='" + ubicacion + '\'' +
                ", descuentoCrc=" + descuentoCrc +
                ", descuentoLong=" + descuentoLong +
                '}';
    }
}
