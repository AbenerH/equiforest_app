package com.equiforestapp.guidu.equiforestapp.Modelo.Views;

import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;

public class DetalleContenedor_View extends DetalleContenedor {

    private int consecutivo;
    private String ubicacion;
    private String fecha;
    private String clase;

    public int getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(int consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }
}
