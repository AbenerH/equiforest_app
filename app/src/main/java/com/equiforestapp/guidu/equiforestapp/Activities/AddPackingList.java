package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_PackingList_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class AddPackingList extends AppCompatActivity implements ITableActivity {

    private static String Id;
    Contenedor contenedor;

    private Boolean found = false;
    private Boolean state = false;

    TextView ubicacion, fecha, clase, consecutivo;

    Button asignar, detalles;

    private TableView<String[]> tableView;
    private Table_PackingList_Helper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_packinglist);

        contenedor = getIntent().getParcelableExtra("Contenedor");

        initialize();
        showData();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Id = "";

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_agregar_viaje);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("Id", Id);
        outState.putString("ubicacion", ubicacion.getText().toString());
        outState.putString("fecha", fecha.getText().toString());
        outState.putString("clase", clase.getText().toString());
        outState.putString("consecutivo", consecutivo.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        ubicacion.setText(savedInstanceState.getString("ubicacion"));
        fecha.setText(savedInstanceState.getString("fecha"));
        clase.setText(savedInstanceState.getString("clase"));
        consecutivo.setText(savedInstanceState.getString("consecutivo"));

        Id = savedInstanceState.getString("Id");

    }

    private void initialize() {

        ubicacion = findViewById(R.id.contenedor_packingubicacion_text);
        fecha = findViewById(R.id.contenedor_packingfecha_text);
        clase = findViewById(R.id.contenedor_packingclase_text);
        consecutivo = findViewById(R.id.contenedor_packingconsecutivo_text);

        asignar = findViewById(R.id.contenedor_asignarPacking_button);
        detalles = findViewById(R.id.contenedor_packingdetalle_button);

        searchContainersPL();

        changeState();


        asignar.setOnClickListener(view -> {

            Log.d("Debugging", "ID PackingList: " + Id);

            if(new DBAdapter(AddPackingList.this).updatePackingListContenedor(Id, contenedor.getContenedorId())){

                if(new DBAdapter(AddPackingList.this).updateContenedorPackingList(Id, contenedor.getContenedorId())){

                    Toast.makeText(AddPackingList.this, "Packing List asignado", Toast.LENGTH_SHORT).show();
                    found = true;
                    state = true;
                    changeState();
                    refreshTable();

                }else{

                    Toast.makeText(AddPackingList.this, "Error al actualizar contenedor", Toast.LENGTH_SHORT).show();

                }

            }else{

                Toast.makeText(AddPackingList.this, "Error al asignar Packing List", Toast.LENGTH_SHORT).show();
            }
        });

        detalles.setOnClickListener(view -> {

            String packingId = Id;

            Intent intent = new Intent(AddPackingList.this, ContenedorInfo.class);
            intent.putExtra("Contenedor", contenedor);
            intent.putExtra("PackingId", packingId);
            startActivity(intent);
        });

    }

    private void changeState() {

        if (!found || !state){
            detalles.setEnabled(false);
            asignar.setEnabled(true);
        }else{
            asignar.setEnabled(false);
            detalles.setEnabled(true);
        }


    }

    private void searchContainersPL() {

        if(new DBAdapter(AddPackingList.this).checkPackingListContainer(contenedor.getContenedorId())){

            found = true;
            state = true;
            Log.d("Debugging", "Se encontro un PackingList con este contenedor: " + contenedor);
            setData();
        }else{
            found = false;
        }

    }

    private void setData() {

        PackingList packingList = new DBAdapter(AddPackingList.this).getContainerPackingList(contenedor.getContenedorId());
        Id = packingList.getPackingListId();
        ubicacion.setText(packingList.getUbicacion());
        fecha.setText(packingList.getFecha());
        clase.setText(packingList.getClase());
        consecutivo.setText(String.valueOf(packingList.getConsecutivo()));

    }

    @Override
    public void showData() {

        tableHelper = new Table_PackingList_Helper(this);
        tableView = findViewById(R.id.contenedor_packing_tableView);
        tableView.setColumnCount(4);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders(Headers.SIMPLE_HEADER)));
        refreshTable();

        tableView.addDataClickListener((rowIndex, clickedData) -> {

            Id = (clickedData[4]);
            ubicacion.setText(clickedData[0]);
            consecutivo.setText(clickedData[1]);
            fecha.setText(clickedData[2]);
            clase.setText(clickedData[3]);

            state = false;
            changeState();

         });
    }

    private void refreshTable() {
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getPackingListsDisponibles()));
    }
}
