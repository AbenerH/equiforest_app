package com.equiforestapp.guidu.equiforestapp.Modelo;

public abstract class TrozaCalculation {

    private static double smalian;
    private static double diametro;

    public static double getDiametro(double circ) {

        diametro = (circ / 3.1416);
        return diametro;
    }

    public static double getSmalian(double circ, double largo) {

        smalian = ((getDiametro(circ) / 100) * (getDiametro(circ) / 100) / 4 * 3.1416 * largo);
        return smalian;
    }

}