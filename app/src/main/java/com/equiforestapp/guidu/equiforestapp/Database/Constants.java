package com.equiforestapp.guidu.equiforestapp.Database;

class Constants {

    //DATABASE
    static final String DATABASE_NAME = "database.db";

    //VERSION
    public static final int VERSION_DB = 1;

    /*                        TABLES                          */

    //Cosechas
    public static final String TABLE_TROZAS = "trozas";
    public static final String TABLE_VIAJES = "viajes";
    public static final String TABLE_RODALES = "rodales";
    public static final String TABLE_UBICACIONES = "ubicaciones";

    //Comercializacion
    public static final String TABLE_FACTURACONTENEDOR = "facturas_contenedor";
    public static final String TABLE_FACTURADETALLECONTENEDOR = "factura_detalle_contenedor";
    public static final String TABLE_PACKINGLIST = "packing_list";

    /*      COLUMNS         */

    //Trozas
    public static final String COLUMN_TROZAS_ID = "id";
    public static final String COLUMN_TROZAS_CLASE = "clase";
    public static final String COLUMN_TROZAS_CIRCUNFERENCIA = "circunferencia";
    public static final String COLUMN_TROZAS_LONGITUD = "longitud";
    public static final String COLUMN_TROZAS_VIAJEID = "viajeid";
    public static final String COLUMN_TROZAS_ESTADO = "estado";

    //Viajes

    public static final String COLUMN_VIAJE_ID= "id";
    public static final String COLUMN_VIAJE_NUCLEO = "nucleo";
    public static final String COLUMN_VIAJE_FINCA = "finca";
    public static final String COLUMN_VIAJE_RODAL = "rodal";
    public static final String COLUMN_VIAJE_FECHA_INGRESO = "fechaIngreso";
    public static final String COLUMN_VIAJE_UBICACION = "ubicacion";
    public static final String COLUMN_VIAJE_NVIAJES = "n_viaje";
    public static final String COLUMN_VIAJE_VIAJEID = "viajeid";
    public static final String COLUMN_VIAJE_NOTROZASC = "noTrozasC";
    public static final String COLUMN_VIAJE_ESTADO = "estado";

    //Rodales

    public static final String COLUMN_RODALES_ID = "id";
    public static final String COLUMN_RODALES_FINCA = "finca";
    public static final String COLUMN_RODALES_NUCLEO = "nucleo";
    public static final String COLUMN_RODALES_RODAL = "rodal";

    //Ubicaciones

    public static final String COLUMN_UBICACIONES_ID = "id";
    public static final String COLUMN_UBICACIONES_NOMBRE = "nombre";;

    //--Facturas_Contenedor
    public static final String COLUMN_FCONTENEDOR_ID = "id"; //PK
    public static final String COLUMN_FCONTENEDOR_FACTURA_NO = "facturaNo"; //FK
    public static final String COLUMN_FCONTENEDOR_PACKINGID = "packingListId";
    public static final String COLUMN_FCONTENEDOR_CONTENEDOR = "contenedorId";
    public static final String COLUMN_FCONTENEDOR_CABEZAL = "cabezal"; //Acepta Nulos
    public static final String COLUMN_FCONTENEDOR_CHOFER= "chofer"; //Acepta Nulos
    public static final String COLUMN_FCONTENEDOR_SELLO = "sello"; //Acepta Nulos
    public static final String COLUMN_FCONTENEDOR_FECHASALIDA = "fechaSalida"; //Acepta Nulos
    public static final String COLUMN_FCONTENEDOR_MAXCARGOWGT = "maxcargoWGT";
    public static final String COLUMN_FCONTENEDOR_ESTADO = "estado";

    //--DetalleContenedor
    public static final String COLUMN_FDETALLECONTENEDOR_ID = "id"; //PK
    public static final String COLUMN_FDETALLECONTENEDOR_PACKINGLIST = "packingListId"; //FK
    public static final String COLUMN_FDETALLECONTENEDOR_CIRCMEDIA = "circunferencia";
    public static final String COLUMN_FDETALLECONTENEDOR_LONG = "longitud";
    public static final String COLUMN_FDETALLECONTENEDOR_ESTADO = "estado";

    //--PackingList
    public static final String COLUMN_PACKINGLIST_ID = "id"; //PK
    public static final String COLUMN_PACKINGLIST_FECHA = "fecha";
    public static final String COLUMN_PACKINGLIST_CONSECUTIVO = "consecutivo";
    public static final String COLUMN_PACKINGLIST_CLASE = "clase";
    public static final String COLUMN_PACKINGLIST_PACKINGID = "packingListId"; //Ubicacion + Fecha + Consecutivo + clase
    public static final String COLUMN_PACKINGLIST_CONTENEDOR = "contenedorId";
    public static final String COLUMN_PACKINGLIST_DESCUENTOCRC= "descuentoCrc";
    public static final String COLUMN_PACKINGLIST_DESCUENTOLONG= "descuentoLong";
    public static final String COLUMN_PACKINGLIST_UBICACION = "ubicacion";
    public static final String COLUMN_PACKINGLIST_ESTADO = "estado";

    //VIEWS
    public static final String VIEW_TROZAS_VIAJES = "vista_trozas_viajes";
    public static final String VIEW_TROZAS_COMERCIALIZACION = "vista_trozas_comercializacion";

    //QUERIES
    public static final String CREATE_TABLE_TROZAS = ("CREATE TABLE " + TABLE_TROZAS + "(" + COLUMN_TROZAS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TROZAS_CLASE + " TEXT, "
            + COLUMN_TROZAS_CIRCUNFERENCIA + " REAL, "
            + COLUMN_TROZAS_LONGITUD + " REAL, "
            + COLUMN_TROZAS_VIAJEID + " TEXT, "
            + COLUMN_TROZAS_ESTADO + " INTEGER )");

    public static final String DROP_TABLE_TROZAS = ("DROP TABLE IF EXISTS " + TABLE_TROZAS);

    public static final String CREATE_TABLE_VIAJE = ("CREATE TABLE " + TABLE_VIAJES + "( " + COLUMN_VIAJE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_VIAJE_NUCLEO + " TEXT, "
            + COLUMN_VIAJE_FINCA + " TEXT, "
            + COLUMN_VIAJE_RODAL + " TEXT,"
            + COLUMN_VIAJE_FECHA_INGRESO + " TEXT,"
            + COLUMN_VIAJE_UBICACION + " TEXT,"
            + COLUMN_VIAJE_NVIAJES + " INTEGER, "
            + COLUMN_VIAJE_VIAJEID + " TEXT, "
            + COLUMN_VIAJE_NOTROZASC + " INTEGER, "
            + COLUMN_VIAJE_ESTADO + " INTEGER ) ");

    public static final String DROP_TABLE_VIAJES = ("DROP TABLE IF EXISTS " + TABLE_VIAJES);

    public static final String CREATE_TABLE_RODALES = ("CREATE TABLE " + TABLE_RODALES + "(" + COLUMN_RODALES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_RODALES_FINCA + " TEXT, "
            + COLUMN_RODALES_NUCLEO + " REAL, "
            + COLUMN_RODALES_RODAL + " TEXT )");

    public static final String DROP_TABLE_RODALES = ("DROP TABLE IF EXISTS " + TABLE_RODALES);

    public static final String CREATE_TABLE_UBICACIONES = ("CREATE TABLE " + TABLE_UBICACIONES + "(" + COLUMN_UBICACIONES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_UBICACIONES_NOMBRE + " TEXT )");

    public static final String DROP_TABLE_UBICACIONES = ("DROP TABLE IF EXISTS " + TABLE_UBICACIONES);

    public static final String CREATE_VIEW_TROZAS_VIAJES = ("CREATE VIEW " + VIEW_TROZAS_VIAJES
            + " AS " + "SELECT a.id,\n" +
            "b.finca, \n" +
            "b.rodal,\n" +
            "b.fechaIngreso,\n" +
            "a.clase,\n" +
            "a.circunferencia,\n" +
            "a.longitud,\n" +
            "a.estado\n" +
            "FROM trozas a \n" +
            "INNER JOIN viajes b on b.viajeid = a.viajeid");

    public static final String DROP_VIEW_TROZAS_VIAJES = ("DROP VIEW IF EXISTS " + VIEW_TROZAS_VIAJES);

    public static final String CREATE_VIEW_TROZAS_COMERCIALIZACION = ("CREATE VIEW " + VIEW_TROZAS_COMERCIALIZACION
            + " AS " + "SELECT a.id,\n" +
           "b.consecutivo, \n" +
            "b.clase, \n" +
           "b.fecha, \n" +
           "b.ubicacion, \n" +
           "a.circunferencia, \n" +
           "a.longitud ,\n" +
            "a.estado \n" +
            " FROM " + TABLE_FACTURADETALLECONTENEDOR + " a \n" +
            "INNER JOIN " + TABLE_PACKINGLIST + " b on b." + COLUMN_PACKINGLIST_PACKINGID + " = a." + COLUMN_FDETALLECONTENEDOR_PACKINGLIST);


    public static final String DROP_VIEW_TROZAS_COMERCIALIZACION = ("DROP VIEW IF EXISTS " + VIEW_TROZAS_COMERCIALIZACION);

    public static final String CREATE_TABLE_FACTURACONTENEDOR = ("CREATE TABLE " + TABLE_FACTURACONTENEDOR + " ( " + COLUMN_FCONTENEDOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_FCONTENEDOR_FACTURA_NO + " TEXT, "
            + COLUMN_FCONTENEDOR_PACKINGID + " TEXT, "
            + COLUMN_FCONTENEDOR_CONTENEDOR + " TEXT, "
            + COLUMN_FCONTENEDOR_CABEZAL + " TEXT, "
            + COLUMN_FCONTENEDOR_CHOFER + " TEXT, "
            + COLUMN_FCONTENEDOR_SELLO + " TEXT, "
            + COLUMN_FCONTENEDOR_FECHASALIDA + " TEXT, "
            + COLUMN_FCONTENEDOR_MAXCARGOWGT + " REAL, "
            + COLUMN_FCONTENEDOR_ESTADO + " INTEGER ) " );

    public static final String DROP_TABLE_FACTURACONTENEDOR = ("DROP TABLE IF EXISTS " + TABLE_FACTURACONTENEDOR);

    public static final String CREATE_TABLE_FACTURADETALLECONTENEDOR = ("CREATE TABLE " + TABLE_FACTURADETALLECONTENEDOR + " ( " + COLUMN_FDETALLECONTENEDOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_FDETALLECONTENEDOR_PACKINGLIST + " TEXT, "
            + COLUMN_FDETALLECONTENEDOR_CIRCMEDIA + " REAL, "
            + COLUMN_FDETALLECONTENEDOR_LONG + " REAL, "
            + COLUMN_FDETALLECONTENEDOR_ESTADO + " INTEGER ) " );

    public static final String DROP_TABLE_FACTURADETALLECONTENEDOR = ("DROP TABLE IF EXISTS " + TABLE_FACTURADETALLECONTENEDOR);

    public static final String CREATE_TABLE_PACKINGLIST = ("CREATE TABLE " + TABLE_PACKINGLIST + " ( " + COLUMN_PACKINGLIST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_PACKINGLIST_FECHA + " TEXT, "
            + COLUMN_PACKINGLIST_CONSECUTIVO + " INTEGER, "
            + COLUMN_PACKINGLIST_CLASE + " TEXT, "
            + COLUMN_PACKINGLIST_CONTENEDOR + " TEXT, "
            + COLUMN_PACKINGLIST_DESCUENTOCRC + " REAL,"
            + COLUMN_PACKINGLIST_DESCUENTOLONG + " REAL,"
            + COLUMN_PACKINGLIST_UBICACION + " TEXT, "
            + COLUMN_PACKINGLIST_PACKINGID + " TEXT, "
            + COLUMN_PACKINGLIST_ESTADO + " INTEGER) " );

    public static final String DROP_TABLE_PACKINGLIST = ("DROP TABLE IF EXISTS " + TABLE_PACKINGLIST);
}
