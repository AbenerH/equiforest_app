package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.Modelo.TrozaCalculation;
import com.equiforestapp.guidu.equiforestapp.Modelo.Views.DetalleContenedor_View;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Table_FContenedorDetalle_Helper {

    Context context;

    private String [] headers;
    private String[][] data;

    public Table_FContenedorDetalle_Helper(Context context) {
        this.context = context;
    }

    public String[] getHeaders(Headers header_type) {

        switch (header_type){

            case SIMPLE_HEADER:
                headers = new String[]{"#", "Circunferencia" , "Longitud", "Diametro", "Smalian"};
                break;

            case DETAIL_HEADER:
                headers = new String[]{"#", "Ubicacion", "Fecha" , "No.Packing", "Clase", "Circunferencia", "Longitud"};
                break;
        }

        return headers;
    }

    public String [][]getDetalleContenedor(String packingid){

        ArrayList<DetalleContenedor> listadoDetallesContenedores = new DBAdapter(context).recuperarListaDetalleContenedoresxPackingList(packingid);
        DetalleContenedor detalleContenedor;

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        data = new String[listadoDetallesContenedores.size()][6];

        for (int i = 0; i < listadoDetallesContenedores.size() ; i++) {

            detalleContenedor = listadoDetallesContenedores.get(i);

            Double circunferencia = detalleContenedor.getCircunferencia();
            Double largo = detalleContenedor.getLongitud();

            data[i][0] = String.valueOf(i+1);
            data[i][1] = String.valueOf(circunferencia);
            data[i][2] = String.valueOf(largo);
            data[i][3] = String.valueOf(decimalFormat.format(detalleContenedor.getDiametro()));
            data[i][4] = String.valueOf(decimalFormat.format(detalleContenedor.getSmalian()));
            data[i][5] = String.valueOf(detalleContenedor.getId());

        }

        return data;
    }

    public String [][] getVistaDetalleContenedores(){

        ArrayList<DetalleContenedor_View> listadoDetallesContenedores = new DBAdapter(context).recuperarVistaTrozasComercializacion();
        DetalleContenedor_View detalle;

//        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        data = new String[listadoDetallesContenedores.size()][7];

        for (int i = 0; i < listadoDetallesContenedores.size() ; i++) {

            detalle = listadoDetallesContenedores.get(i);

            double circunferencia = detalle.getCircunferencia();
            double largo = detalle.getLongitud();

            data[i][0] = String.valueOf(i+1);
            data[i][1] = String.valueOf(detalle.getUbicacion());
            data[i][2] = detalle.getFecha();
            data[i][3] = String.valueOf(detalle.getConsecutivo());
            data[i][4] = detalle.getClase();
            data[i][5] = String.valueOf(circunferencia);
            data[i][6] = String.valueOf(largo);

        }

        return data;
    }

}
