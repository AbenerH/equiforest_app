package com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Trozas_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class Troza_TablaDetalle extends AppCompatActivity implements ITableActivity {

    String viajeId;

    TableView<String[]> tableView;
    Table_Trozas_Helper tableTrozasHelper;
    TextView text_volumen;

    double volumen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_troza__tabla_detalle);

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        viajeId = getIntent().getStringExtra("viajeId");

        text_volumen = findViewById(R.id.text_volumen);
        volumen = new DBAdapter(this).getVolumenTotalxViaje(viajeId);
        text_volumen.setText((String.valueOf(decimalFormat.format(volumen))));

        showData();

    }

    @Override
    public void showData() {

        tableTrozasHelper = new Table_Trozas_Helper(this);
        tableView = (TableView<String[]>) findViewById(R.id.tableView_trozas_detalle);
        TableColumnWeightModel columnModel = new TableColumnWeightModel(6);
        columnModel.setColumnWeight(0, 1);
        columnModel.setColumnWeight(1, 1);
        columnModel.setColumnWeight(2, 2);
        columnModel.setColumnWeight(3, 2);
        columnModel.setColumnWeight(4, 2);
        columnModel.setColumnWeight(5, 2);
        tableView.setColumnModel(columnModel);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableTrozasHelper.getHeaders(Headers.SIMPLE_HEADER)));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableTrozasHelper.getTrozasxID(viajeId)));

    }
}
