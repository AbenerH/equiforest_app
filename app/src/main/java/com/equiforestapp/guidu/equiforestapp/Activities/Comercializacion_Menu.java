package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;

import com.equiforestapp.guidu.equiforestapp.API_Process_Comercializacion;
import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.Comercializacion_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.R;

public class Comercializacion_Menu extends AppCompatActivity {

//    Button packing, ver, contenedores, subir;
    CardView packing, ver, contenedores, subir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comercializacion__menu);

        initialize();

    }

    private void initialize() {

        packing = findViewById(R.id.packingLists_cardView);
        ver = findViewById(R.id.detalleContenedores_cardView);
        contenedores = findViewById(R.id.contenedores_cardView);
        subir = findViewById(R.id.subir_cardView);

        packing.setOnClickListener(new View.OnClickListener() {

            private long mLastClickTime = 0;

            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Intent intent = new Intent(Comercializacion_Menu.this, CrearPackingList.class);
                startActivity(intent);
            }
        });

        ver.setOnClickListener(new View.OnClickListener() {

            private long mLastClickTime = 0;

            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Intent intent = new Intent(Comercializacion_Menu.this, Comercializacion_TablaDetalle.class);
                startActivity(intent);
            }
        });

        contenedores.setOnClickListener(new View.OnClickListener() {

            private long mLastClickTime = 0;

            @Override
            public void onClick(View view) {


                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Intent intent = new Intent(Comercializacion_Menu.this, CrearContenedor.class);
                startActivity(intent);
            }
        });
        
        subir.setOnClickListener(new View.OnClickListener() {

            private long mLastClickTime = 0;

            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                API_Process_Comercializacion apiProcess = new API_Process_Comercializacion(Comercializacion_Menu.this);
                apiProcess.startUpload();

            }
        });
    }
}
