package com.equiforestapp.guidu.equiforestapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.IMyAPI;
import com.equiforestapp.guidu.equiforestapp.Modelo.Rodal;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.Modelo.Viaje;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class API_Process_Cosechas {

    private Context context;
    private IMyAPI myAPI;

    private StringBuilder message;
    private static int elements;
    private static float progress;
    private static float sum;

    private Boolean updated;


    public API_Process_Cosechas(Context context) {
        this.context = context;

    }

    public void StartDownload() {

            if (CheckConnection()) {

                Toast.makeText(context, "Conexion a Internet NO disponible, intentelo más tarde!", Toast.LENGTH_SHORT).show();

            } else {

                DownloadData();
            }
    }
/*
        if (resultRodales == 138) {

            Log.d("Debugging", "Rodales ya cargados");
            Toast.makeText(context, "Todos los datos de Rodales ya estan cargados, no hay necesidad de descargarlos de nuevo", Toast.LENGTH_SHORT).show();
*/


    public void StartUpload() {

        if (CheckConnection()) {
            Toast.makeText(context, "Conexion a Internet NO disponible, intentelo más tarde!", Toast.LENGTH_SHORT).show();

        } else {

            int resultViajes = new DBAdapter(context).recuperarListaViajes(0).size();
            int resultTrozas = new DBAdapter(context).recuperarListaTrozas(0).size();

            if (resultViajes == 0 && resultTrozas == 0) {

                Toast.makeText(context, "No hay registros nuevos que subir a la nube", Toast.LENGTH_SHORT).show();

            } else {

                uploadData();
            }
        }
    }

    private void DownloadData() {

        int resultRodales = new DBAdapter(context).recuperarListaRodales().size();

        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        boolean firstTimedownload = preferences.getBoolean("rodalesDownload", true);

        if (firstTimedownload) {

            android.support.v7.app.AlertDialog.Builder alerta = new android.support.v7.app.AlertDialog.Builder(context);
            alerta.setMessage("¿Desea descargar los datos de rodales? Si existen rodales creados por el usuario serán sobreescritos")
                    .setCancelable(false)
                    .setPositiveButton("Si", (dialogInterface, i) -> {

                        //Overwriting data
                        if (resultRodales != 0) {

                            if (new DBAdapter(context).eliminarRodales()) {

                                Log.d("Debugging", "Rodales sobrescritos exitosamente");
                                Toast.makeText(context, "Rodales sobrescritos", Toast.LENGTH_SHORT).show();

                            } else {

                                Log.e("Debugging", "Error al sobreescribir los rodales");
                                Toast.makeText(context, "Error al sobrescribir los rodales", Toast.LENGTH_SHORT).show();
                            }
                        }

                        //Create Retrofit Instance
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://equiforest1-001-site1.dtempurl.com/")
                                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        myAPI = retrofit.create(IMyAPI.class);

                        Log.d("Debugging", "Flag 1");

                        message = new StringBuilder();

                        Disposable disposable = myAPI.getRodales()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<ArrayList<Rodal>>() {
                                    @Override
                                    public void onSuccess(ArrayList<Rodal> rodals) {

                                        Log.d("Debugging", "Flag 2");

                                        boolean uploaded = new DBAdapter(context).guardarRodales(rodals);

                                        if (uploaded) {

                                            Log.d("Debugging", "Flag 3");

                                            Log.d("Debugging", "Rodales descargados y añadidos exitosamente");
                                            Toast.makeText(context, "Rodales descargados y añadidos exitosamente", Toast.LENGTH_SHORT).show();

                                            SharedPreferences.Editor editor = preferences.edit();
                                            editor.putBoolean("rodalesDownload", false);
                                            editor.apply();

                                        } else {

                                            Log.d("Debugging", "Flag 2.5");

                                            Log.d("Debugging", "Error al añadir rodales");
                                            Toast.makeText(context, "Error al añadir rodales", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                        Log.d("Debugging", "Flag 5");

                                        Log.e("Error", "Error al descargar rodales" + e.getMessage() + " " + e.getCause());
                                        Toast.makeText(context, "Error al descargar rodales " + e.getMessage() + " " + e.getCause(), Toast.LENGTH_SHORT).show();

                                    }
                                });
                    })
                    .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

            android.support.v7.app.AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Bajar de la nube");
            myAlert.show();

        }else {

            Toast.makeText(context, "Rodales ya deberian haber sido descargados", Toast.LENGTH_SHORT).show();

        }

    }

    private void uploadData() {

        android.support.v7.app.AlertDialog.Builder alerta = new android.support.v7.app.AlertDialog.Builder(context);
        alerta.setMessage("¿Desea subir estos registros a la nube? Debe asegurarse que todo esta correctamente asignado!")
                .setCancelable(false)
                .setPositiveButton("Si", (dialogInterface, i) -> {

                    //Do nothing

                })
                .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

        android.support.v7.app.AlertDialog myAlert = alerta.create();
        myAlert.setTitle("Subir a la nube");
        myAlert.show();

        myAlert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view -> {

            if (checkServerSide()) {

                Log.d("Debugging", "TRUE");

            } else {

                Log.d("Debugging", "FALSE");

                /*

                myAlert.dismiss();
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Subiendo...");
                progressDialog.setMessage("Espere mientras se suben todos los registros");
                progressDialog.setCancelable(false);

                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

                progressDialog.setIcon(R.drawable.upload_icon);

                progressDialog.setMax(100);

                //setting the OK Button
                progressDialog.setButton(ProgressDialog.BUTTON_POSITIVE, "OK", (dialog, whichButton) -> progressDialog.dismiss());

                //Showing Dialog
                progressDialog.show();

                elements = 0;

                //Disabling OK Button
                progressDialog.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(false);

                //Create Retrofit Instance
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://equiforest1-001-site1.dtempurl.com/")
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                myAPI = retrofit.create(IMyAPI.class);

                message = new StringBuilder();

                Observable<ArrayList<Viaje>> viajesObservable = myAPI.addViajes(getViajes())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

                elements++;

                Observable<ArrayList<Troza>> trozasObservable = myAPI.addTrozas(getTrozas())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

                elements++;


                sum = (float) 100 / elements;
                progress = 0;

                Observable.concat(viajesObservable, trozasObservable).subscribe(new Observer<ArrayList<?>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        //Do Nothing
                    }


                    @Override
                    public void onNext(ArrayList<?> objects) {

                        Log.d("API_Process", "Data uploaded " + objects.toString());

                        progress += sum;

                        if (objects.size() != 0) {

                            String name = objects.get(0).getClass().getSimpleName();

                            switch (name) {
                                case "Viaje":
                                    updateViajes();
                                    break;

                                case "Troza":
                                    updateTrozas();
                                    break;

                                default:
                                    Toast.makeText(context, "Dato desconocido", Toast.LENGTH_SHORT).show();
                                    break;
                            }

                            message.append(objects.size()).append(" datos de ").append(name).append(" subidos ").append("\n");
                            progressDialog.setMessage(message);
                            progressDialog.setProgress((int) progress);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e("API_Process", "Error at uploading data: " + e.getMessage() + e.getCause());
                        message.append("Error al subir datos: ").append(e.getCause()).append(" ").append(e.getMessage()).append(e.toString());
                        progressDialog.setMessage(message);
                        progressDialog.setTitle("Error al subir");
                        progressDialog.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(true);

                    }

                    @Override
                    public void onComplete() {

                        message.append("\n Todos los datos han sido subidos exitosamente ");
                        myAlert.setMessage(message);
                        progressDialog.setTitle("Datos subidos exitosamente");
                        progressDialog.setMessage(message);
                        progressDialog.setProgress(100);
                        progressDialog.setCancelable(true);
                        progressDialog.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(true);
                        Log.d("API_Process", "OnCompleted");

                    }

                });
                    */
            }
        });

    }

    private boolean checkServerSide() {

        boolean available = false;

        try {

            available = InetAddress.getByName("http://equiforest1-001-site1.dtempurl.com/").isReachable(10000);

        } catch (IOException e) {

            Log.e("API_Process Exception", e.getMessage());
        }

        Log.d("API_Process ", "Server is available = : " + available);

        return available;
    }


    private ArrayList<Viaje> getViajes() {

        ArrayList<Viaje> viajes = new DBAdapter(context).recuperarListaViajes(0);

        Log.d("Debugging", "Sending: " + viajes.toString());

        return viajes;

    }

    private ArrayList<Troza> getTrozas() {

        ArrayList<Troza> trozas = new DBAdapter(context).recuperarListaTrozas(0);

        Log.d("Debugging", "Sending: " + trozas.toString());

        return trozas;

    }

    private boolean CheckConnection() {
        boolean wifi = false;
        boolean mobileData = false;

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = manager.getAllNetworkInfo();

        for (NetworkInfo info : networkInfos) {
            if (info.getTypeName().equalsIgnoreCase("Wifi"))
                if (info.isConnected())
                    wifi = true;
            if (info.getTypeName().equalsIgnoreCase("MOBILE"))
                if (info.isConnected())
                    mobileData = true;
        }
        return !wifi && !mobileData;
    }


    private void updateViajes() {

        updated = new DBAdapter(context).updateViajesEstado(1);
        Log.d("Debugging", "Viajes updated == " + updated);

    }

    private void updateTrozas() {

        updated = new DBAdapter(context).updateTrozasEstado(1);
        Log.d("Debugging", "Trozas updated == " + updated);

    }

}
