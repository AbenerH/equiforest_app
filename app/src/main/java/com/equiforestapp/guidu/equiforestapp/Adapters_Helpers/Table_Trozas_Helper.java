package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.Modelo.TrozaCalculation;
import com.equiforestapp.guidu.equiforestapp.Modelo.Views.Troza_View;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Table_Trozas_Helper {

    Context context;

    private String [] headers;
    private String[][] data;

    public Table_Trozas_Helper(Context context){
        this.context = context;
    }

    public String[] getHeaders(Headers header) {


        switch (header){

            case SIMPLE_HEADER: headers = new String[]{"#", "Clase", "Circunferencia", "Largo", "Diametro", "Vol. Smalian" ,"ID"};
            break;

            case DETAIL_HEADER: headers = new String[]{"#", "Finca", "Rodal", "clase", "Circunf.", "Largo", "Diametro", "Vol. Smalian"};
            break;

        }

        return headers;
    }

    public String[][] getAllTrozas(){

        ArrayList<Troza_View> listadoTrozas =  new DBAdapter(context).recuperarVistaTrozasViajes();
        Troza_View troza_view;

        data = new String[listadoTrozas.size()][6];

        for (int i = 0; i < listadoTrozas.size() ; i++) {

            troza_view = listadoTrozas.get(i);

            data[i][0] = String.valueOf(i + 1);
            data[i][1] = troza_view.getFinca();
            data[i][2] = troza_view.getRodal();
            data[i][3] = troza_view.getClasificacion();
            data[i][4] = String.valueOf(troza_view.getCircunferencia());
            data[i][5] = String.valueOf(troza_view.getLargo());
        }

        return data;
    }

    public String[][] getTrozasxID(String id){

        ArrayList<Troza> listadoTrozas = new DBAdapter(context).recuperarListaTrozasxViaje(id);
        Troza troza;

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        data = new String[listadoTrozas.size()][7];

        for (int i = 0; i < listadoTrozas.size() ; i++) {

            troza = listadoTrozas.get(i);

            data[i][0] = String.valueOf(i + 1);
            data[i][1] = troza.getClasificacion();
            data[i][2] = String.valueOf(troza.getCircunferencia());
            data[i][3] = String.valueOf(troza.getLargo());
            data[i][4] = String.valueOf(decimalFormat.format(TrozaCalculation.getDiametro(troza.getCircunferencia())));
            data[i][5] = String.valueOf(decimalFormat.format(TrozaCalculation.getSmalian(troza.getCircunferencia(), troza.getLargo())));
            data[i][6] = String.valueOf(troza.getId());

        }

        return data;
    }
}
