package com.equiforestapp.guidu.equiforestapp.Modelo;

import android.os.Parcel;
import android.os.Parcelable;

public class Contenedor implements Parcelable {

    private int id;
    private String facturaNo;
    private String packingListId;
    private String contenedorId;
    private String cabezal;
    private String chofer;
    private String sello;
    private String fechaSalida;
    private double maxCargoWgt;

    public Contenedor() {}

    public Contenedor(int id, String facturaNo, String packingListId, String contenedorId, String cabezal, String chofer, String sello, String fechaSalida, double maxCargoWgt) {
        this.id = id;
        this.facturaNo = facturaNo;
        this.packingListId = packingListId;
        this.contenedorId = contenedorId;
        this.cabezal = cabezal;
        this.chofer = chofer;
        this.sello = sello;
        this.fechaSalida = fechaSalida;
        this.maxCargoWgt = maxCargoWgt;
    }

    protected Contenedor(Parcel in) {
        id = in.readInt();
        facturaNo = in.readString();
        packingListId = in.readString();
        contenedorId = in.readString();
        cabezal = in.readString();
        chofer = in.readString();
        sello = in.readString();
        fechaSalida = in.readString();
        maxCargoWgt = in.readDouble();
    }

    public static final Creator<Contenedor> CREATOR = new Creator<Contenedor>() {
        @Override
        public Contenedor createFromParcel(Parcel in) {
            return new Contenedor(in);
        }

        @Override
        public Contenedor[] newArray(int size) {
            return new Contenedor[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFacturaNo() {
        return facturaNo;
    }

    public void setFacturaNo(String facturaNo) {
        this.facturaNo = facturaNo;
    }

    public String getPackingListId() {
        return packingListId;
    }

    public void setPackingListId(String packingListId) {
        this.packingListId = packingListId;
    }

    public String getContenedorId() {
        return contenedorId;
    }

    public void setContenedorId(String contenedorId) {
        this.contenedorId = contenedorId;
    }

    public String getCabezal() {
        return cabezal;
    }

    public void setCabezal(String cabezal) {
        this.cabezal = cabezal;
    }

    public String getChofer() {
        return chofer;
    }

    public void setChofer(String chofer) {
        this.chofer = chofer;
    }

    public String getSello() {
        return sello;
    }

    public void setSello(String sello) {
        this.sello = sello;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public double getMaxCargoWgt() {
        return maxCargoWgt;
    }

    public void setMaxCargoWgt(double maxCargoWgt) {
        this.maxCargoWgt = maxCargoWgt;
    }

    @Override
    public String toString() {
        return "Contenedor{" +
                "id=" + id +
                ", facturaNo='" + facturaNo + '\'' +
                ", packingListId='" + packingListId + '\'' +
                ", contenedorId='" + contenedorId + '\'' +
                ", cabezal='" + cabezal + '\'' +
                ", chofer='" + chofer + '\'' +
                ", sello='" + sello + '\'' +
                ", fechaSalida='" + fechaSalida + '\'' +
                ", maxCargoWgt=" + maxCargoWgt +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(facturaNo);
        parcel.writeString(packingListId);
        parcel.writeString(contenedorId);
        parcel.writeString(cabezal);
        parcel.writeString(chofer);
        parcel.writeString(sello);
        parcel.writeString(fechaSalida);
        parcel.writeDouble(maxCargoWgt);
    }
}
