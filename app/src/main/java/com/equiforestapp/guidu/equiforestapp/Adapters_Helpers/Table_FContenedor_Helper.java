package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;

import java.util.ArrayList;

public class Table_FContenedor_Helper {

    Context context;

    private String [] headers;
    private String[][] data;

    public Table_FContenedor_Helper(Context context) {
        this.context = context;
    }

    public String[] getHeaders(Headers header_type) {

        switch (header_type){

            case SIMPLE_HEADER:
                headers = new String[]{"#Contenedor", "Cabezal" , "Sello"};
            break;

            case DETAIL_HEADER:
                headers = new String[]{"#Contenedor", "Cabezal" , "Sello", "MaxWG"};
                break;
        }

        return headers;
    }

    public String [][]getContenedores(){

        ArrayList<Contenedor> listadoContenedores = new DBAdapter(context).recuperarListaContenedores(0);
        Contenedor contenedor;

        data = new String[listadoContenedores.size()][7];

        for (int i = 0; i < listadoContenedores.size() ; i++) {

            contenedor = listadoContenedores.get(i);

            data[i][0] = String.valueOf(contenedor.getContenedorId());
            data[i][1] = contenedor.getCabezal();
            data[i][2] = contenedor.getSello();
            data[i][3] = String.valueOf(contenedor.getMaxCargoWgt());
            data[i][4] = contenedor.getFechaSalida();
            data[i][5] = contenedor.getChofer();
            data[i][6] = String.valueOf(contenedor.getId());

        }

        return data;
    }

}
