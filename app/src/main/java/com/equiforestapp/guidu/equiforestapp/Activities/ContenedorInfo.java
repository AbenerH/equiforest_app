package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;
import com.equiforestapp.guidu.equiforestapp.R;

import java.net.IDN;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ContenedorInfo extends AppCompatActivity {

    private static String Id;

    TextView ntrozas, maxWeight, promedio, smalian, hoppus;

    Contenedor contenedor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenedor_info);

        contenedor = getIntent().getParcelableExtra("Contenedor");

        Id = getIntent().getStringExtra("PackingId");

        initialize();
    }

    private void initialize() {

        maxWeight = findViewById(R.id.contenedor_infoMaxWeight_text);
        promedio = findViewById(R.id.contenedor_infoPromedio_text);
        smalian = findViewById(R.id.contenedor_infoSmalian_text);
        hoppus = findViewById(R.id.contenedor_infoHoppus_text);
        ntrozas = findViewById(R.id.contenedor_infotrozas_text);
        
        setData();

    }

    private void setData() {

        maxWeight.setText(String.valueOf(contenedor.getMaxCargoWgt()));

        ArrayList<DetalleContenedor> listado = new DBAdapter(ContenedorInfo.this).recuperarListaDetalleContenedoresxPackingList(String.valueOf(Id));
        PackingList packingList = new DBAdapter(ContenedorInfo.this).recuperarPackingListxID(Id);

        Log.d("Debugging", "Trozas in Packing List ID " + Id + " = " + listado.size());

        double promedio_circunferencia;

        double sumaCircunferencia = 0;
        double sumaSmalian = 0;
        double sumaHoppus = 0;

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        for (int i = 0; i < listado.size() ; i++) {

            double circunf = listado.get(i).getCircunferencia();
            double largo = listado.get(i).getLongitud();

            sumaCircunferencia += circunf;
            sumaSmalian += listado.get(i).getSmalian();
            Log.d("Debugging", "Hoppus of: " + i + " = " + getHoppus(circunf,largo, packingList.getDescuentoCrc(), packingList.getDescuentoLong()));
            sumaHoppus += getHoppus(circunf, largo, packingList.getDescuentoCrc(), packingList.getDescuentoLong());

        }

        promedio_circunferencia = sumaCircunferencia / listado.size();

        ntrozas.setText(String.valueOf(listado.size()));
        promedio.setText(String.valueOf(decimalFormat.format(promedio_circunferencia)));
        smalian.setText(String.valueOf(decimalFormat.format(sumaSmalian)));
        hoppus.setText(String.valueOf(decimalFormat.format(sumaHoppus)));

    }

    public double getHoppus(double circunferencia, double largo, double descuentoCrc, double descuentoLong){

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        double hoppus;
        double circunf_desc;
        double largo_desc;

        double result;

        circunf_desc = (circunferencia / 100) - descuentoCrc;
        largo_desc = largo - descuentoLong;

        result = ((circunf_desc * circunf_desc) * largo_desc) /16;

        String converted = decimalFormat.format(result);

        hoppus = Double.parseDouble(converted);


        return hoppus;


    }

}
