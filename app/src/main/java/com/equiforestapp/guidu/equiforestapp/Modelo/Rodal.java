package com.equiforestapp.guidu.equiforestapp.Modelo;

public class Rodal {

    private int id;
    private String finca;
    private String nucleo;
    private String rodal;


    public Rodal() {
    }

    public Rodal(int id, String finca, String nucleo, String rodal) {
        this.id = id;
        this.finca = finca;
        this.nucleo = nucleo;
        this.rodal = rodal;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFinca() {
        return finca;
    }

    public void setFinca(String finca) {
        this.finca = finca;
    }

    public String getNucleo() {
        return nucleo;
    }

    public void setNucleo(String nucleo) {
        this.nucleo = nucleo;
    }

    public String getRodal() {
        return rodal;
    }

    public void setRodal(String rodal) {
        this.rodal = rodal;
    }

    @Override
    public String toString() {
        return "Rodal{" +
                "id=" + id +
                ", finca='" + finca + '\'' +
                ", nucleo='" + nucleo + '\'' +
                ", rodal='" + rodal + '\'' +
                '}';
    }
}
