package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.DC_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_FContenedorDetalle_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;
import com.equiforestapp.guidu.equiforestapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataLongClickListener;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class FacturaDetalleContenedor extends AppCompatActivity {

    String fecha, consecutivo, clase, packingId;

    TextView fecha_text, consecutivo_text, clase_text, promedio;
    EditText circunferencia, longitud;

    Button  guardar, verTabla;

    private TableView<String[]> tableView;
    private Table_FContenedorDetalle_Helper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura_detalle_contenedor);

        packingId = getIntent().getStringExtra("packingId");
        fecha = getIntent().getStringExtra("fecha");
        consecutivo = getIntent().getStringExtra("consecutivo");
        clase = getIntent().getStringExtra("clase");

        Log.d("Debugging", "PackingId transferido: " + packingId);

        initialize();
        showData();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_agregar_viaje);
    }

    private void initialize() {

        promedio = findViewById(R.id.packing_promedio_total);

        fecha_text = findViewById(R.id.packing_fecha_text);
        consecutivo_text = findViewById(R.id.packing_consecutivo_text);
        clase_text = findViewById(R.id.packing_clase_text);

        fecha_text.setText(fecha);
        consecutivo_text.setText(consecutivo);
        clase_text.setText(clase);

        circunferencia = findViewById(R.id.circunf_edit);
        longitud = findViewById(R.id.largo_edit);

        longitud.setText(R.string.default_trozas_value);

        guardar = findViewById(R.id.guardarFCDTroza_button);
        verTabla = findViewById(R.id.verTablaDC_button);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!checkFields()) {

                    DetalleContenedor detalleContenedor = new DetalleContenedor();
                    detalleContenedor.setCircunferencia(Double.parseDouble(circunferencia.getText().toString()));
                    detalleContenedor.setLongitud(Double.parseDouble(longitud.getText().toString()));
                    detalleContenedor.setPackingListId(packingId);

                    if (new DBAdapter(FacturaDetalleContenedor.this).guardarDetalleContenedor(detalleContenedor)) {

                        Toast.makeText(FacturaDetalleContenedor.this, "Datos insertados", Toast.LENGTH_SHORT).show();
                        cleanfields();
                        refresh();

                    } else {

                        Toast.makeText(FacturaDetalleContenedor.this, "Error al guardar!", Toast.LENGTH_SHORT).show();

                    }

                }else {
                    Toast.makeText(FacturaDetalleContenedor.this, "No pueden haber datos vacios", Toast.LENGTH_SHORT).show();
                }
            }
        });

        verTabla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(FacturaDetalleContenedor.this, DC_TablaDetalle.class);
                intent.putExtra("packingId", packingId);
                startActivity(intent);

            }
        });

    }

    private void actualizarPromedio(){
        promedio.setText(getPromedio());
        Log.d("Debugging", "Promedio actualizado");
    }

    private String getPromedio(){
        ArrayList<DetalleContenedor> listado = new DBAdapter(FacturaDetalleContenedor.this).recuperarListaDetalleContenedoresxPackingList(packingId);
        double promedio = 0;
        double suma = 0;

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        for (int i = 0; i < listado.size() ; i++) {

            suma+= listado.get(i).getCircunferencia();

        }

        promedio = (suma / listado.size());

        return String.valueOf(decimalFormat.format((promedio)));

    }

    private void cleanfields() {
        circunferencia.setText("");
        longitud.setText(R.string.default_trozas_value);
    }

    private boolean checkFields() {

        return circunferencia.getText().toString().equals("") || longitud.getText().toString().equals("");

    }

    private void showData() {

        tableHelper = new Table_FContenedorDetalle_Helper(this);
        tableView = findViewById(R.id.tableView_FCDTrozas);
        tableView.setColumnCount(3);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders(Headers.SIMPLE_HEADER)));

        tableView.addDataLongClickListener(new longClickListenerHandler());

        refresh();
    }

    private void refresh(){
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getDetalleContenedor(packingId)));
        actualizarPromedio();

    }

    private class longClickListenerHandler implements TableDataLongClickListener<String[]> {

        @Override
        public boolean onDataLongClicked(int rowIndex, String[] clickedData) {

            final int id =  Integer.parseInt(((String[]) clickedData)[5]);

            AlertDialog.Builder alerta = new AlertDialog.Builder(FacturaDetalleContenedor.this);
            alerta.setMessage("¿Desea borrar este registro?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            if (new DBAdapter(FacturaDetalleContenedor.this).eliminarDetalleContenedor(id)) {

                                Toast.makeText(FacturaDetalleContenedor.this, "Registro Borrado", Toast.LENGTH_SHORT).show();
                                refresh();

                            } else {
                                Toast.makeText(FacturaDetalleContenedor.this, "El registro no se pudo borrar", Toast.LENGTH_SHORT).show();
                            }

                            dialogInterface.dismiss();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Borrar Troza");
            myAlert.show();

            return true;
        }
    }
}


