package com.equiforestapp.guidu.equiforestapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.IMyAPI;
import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class API_Process_Comercializacion {

    private Context context;
    private IMyAPI myAPI;

    private StringBuilder message;
    private static int elements;
    private static float progress;
    private static float sum;

    private Boolean updated;

    public API_Process_Comercializacion(Context context) {
        this.context = context;

    }

    public void startUpload() {


        if (!CheckConnection()) {

            Toast.makeText(context, "Conexion a Internet NO disponible, intentelo más tarde!", Toast.LENGTH_SHORT).show();

        } else {

            int resultDC = getDetalleContenedores().size();
            int resultCon = getContenedores().size();

            Log.d("Debugging", "Listado DetalleContenedor: " + resultDC);
            Log.d("Debugging", "Listado Contenedor: " + resultCon);

            if (resultDC == 0 && resultCon == 0) {

                Toast.makeText(context, "No hay registros nuevos que enviar a la nube", Toast.LENGTH_SHORT).show();

            } else {

                UploadData();
            }
        }
    }

    private void UploadData() {

        android.support.v7.app.AlertDialog.Builder alerta = new android.support.v7.app.AlertDialog.Builder(context);
        alerta.setMessage("¿Desea subir estos registros a la nube? Debe asegurarse que todo esta correctamente asignado!")
                .setCancelable(false)
                .setPositiveButton("Si", (dialogInterface, i) -> {

                    //Overwrite this

                })
                .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

        android.support.v7.app.AlertDialog myAlert = alerta.create();
        myAlert.setTitle("Subir a la nube");
        myAlert.show();


        myAlert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view -> {

                myAlert.dismiss();

                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle("Subiendo...");
                progressDialog.setMessage("Espere mientras se suben todos los registros");
                progressDialog.setCancelable(false);

                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

                progressDialog.setIcon(R.drawable.upload_icon);

                progressDialog.setMax(100);

                //setting the OK Button
                progressDialog.setButton(ProgressDialog.BUTTON_POSITIVE, "OK", (dialog, whichButton) -> progressDialog.dismiss());

                //Showing Dialog
                progressDialog.show();

                elements = 0;

                //Disabling OK Button
                progressDialog.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(false);

                //Create Retrofit Instance
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://equiforest1-001-site1.dtempurl.com/")
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                myAPI = retrofit.create(IMyAPI.class);

                message = new StringBuilder();

                Observable<ArrayList<PackingList>> packingListObservable = myAPI.addPackingLists(getPackingLists())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

                elements++;

                Observable<ArrayList<DetalleContenedor>> detalleContenedorObservable = myAPI.addDetalleContenedor(getDetalleContenedores())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

                elements++;


                Observable<ArrayList<Contenedor>> contenedorObservable = myAPI.addContenedores(getContenedores())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());

                elements++;

                sum = (float) 100 / elements;

                progress = 0;

                Observable.concat(packingListObservable, detalleContenedorObservable, contenedorObservable).subscribe(new Observer<ArrayList<?>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ArrayList<?> objects) {
                        Log.d("API_Process", "Data uploaded " + objects.toString());

                        progress += sum;

                        if (objects.size() != 0) {

                            String name = objects.get(0).getClass().getSimpleName();

                            switch (name) {
                                case "PackingList":
                                    updatePackingList();
                                    break;

                                case "DetalleContenedor":
                                    updateDetalleContenedor();
                                    break;

                                case "Contenedor":
                                    updateContenedor();
                                    break;
                                default:
                                    Toast.makeText(context, "No reconocido", Toast.LENGTH_SHORT).show();
                                    break;
                            }

                            message.append(objects.size()).append(" datos de ").append(name).append(" enviados ").append("\n");
                            progressDialog.setMessage(message);
                            progressDialog.setProgress((int) progress);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("API_Process", "Error at uploading data: " + e.getMessage() + e.getCause());
                        message.append("Error al subir datos: ").append(e.getCause()).append(" ").append(e.getMessage());
                        progressDialog.setMessage(message);
                        progressDialog.setTitle("Error al subir");
                        progressDialog.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(true);
                    }


                    @Override
                    public void onComplete() {
                        message.append("\n Todos los datos han sido enviados exitosamente ");
                        progressDialog.setTitle("Datos enviados exitosamente");
                        progressDialog.setMessage(message);
                        progressDialog.setProgress(100);
                        progressDialog.setCancelable(true);
                        progressDialog.getButton(ProgressDialog.BUTTON_POSITIVE).setEnabled(true);
                        Log.d("API_Process", "OnCompleted");

                    }
                });
        });

    }

    private void updatePackingList() {

        updated = new DBAdapter(context).updatePackingListEstado(1);
        Log.d("Debugging", "PackingLists updated == " + updated);

    }

    private void updateDetalleContenedor() {

        updated = new DBAdapter(context).updateDetalleContenedoresEstado(1);
        Log.d("Debugging", "DetalleContenedores updated == " + updated);

    }

    private void updateContenedor() {

        updated = new DBAdapter(context).updateContenedoresEstado(getContenedores());
        Log.d("Debugging", "Contenedores updated == " + updated);
    }

    private ArrayList<PackingList> getPackingLists() {

        ArrayList<PackingList> packingLists = new DBAdapter(context).recuperarListaPackingList(0);

        Log.d("Debugging", "Sending: " + packingLists.toString());

        return packingLists;

    }

    private ArrayList<DetalleContenedor> getDetalleContenedores() {

        ArrayList<DetalleContenedor> detalleContenedores = new DBAdapter(context).recuperarListaDetalleContenedores(0);

        Log.d("Debugging", "Sending: " + detalleContenedores.toString());

        return detalleContenedores;

    }

    private ArrayList<Contenedor> getContenedores() {

        ArrayList<Contenedor> contenedores = new DBAdapter(context).recuperarListaContenedores(0);

        for (Contenedor container : contenedores) {

            if (container.getPackingListId() == null || container.getCabezal().equals("") || container.getChofer().equals("") || container.getSello().equals("") || container.getFechaSalida().equals("")) {
                contenedores.remove(container);
            }
        }

        Log.d("Debugging", "Sending: " + contenedores.toString());

        return contenedores;

    }

    private boolean CheckConnection() {
        boolean wifi = false;
        boolean mobileData = false;

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = manager.getAllNetworkInfo();

        for (NetworkInfo info : networkInfos) {
            if (info.getTypeName().equalsIgnoreCase("Wifi"))
                if (info.isConnected())
                    wifi = true;
            if (info.getTypeName().equalsIgnoreCase("MOBILE"))
                if (info.isConnected())
                    mobileData = true;
        }
        return wifi || mobileData;
    }

}
