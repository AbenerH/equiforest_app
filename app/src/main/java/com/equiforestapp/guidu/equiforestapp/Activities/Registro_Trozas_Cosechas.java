package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.Troza_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Trozas_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class Registro_Trozas_Cosechas extends AppCompatActivity implements ITableActivity {

    int getNViaje;
    String viajeid;
    String getFinca, getRodal;

    TextView  finca, rodal, viaje;
    EditText circunf, largo;

    Spinner clases;

    Button guardar, verTabla;

    TableView<String[]> tableView;
    Table_Trozas_Helper tableHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_trozas);

        viajeid = getIntent().getStringExtra("viajeId");
        getFinca = getIntent().getStringExtra("finca");
        getRodal= getIntent().getStringExtra("rodal");
        getNViaje = getIntent().getIntExtra("nViaje", 0);

        Log.d("Debugging", "Viaje Id transferido: " + viajeid);

        initialize();
        showData();

    }

    @Override
    public void showData() {

        tableHelper = new Table_Trozas_Helper(this);
        tableView = (TableView<String[]>)findViewById(R.id.tableView_trozas);
        tableView.setColumnCount(4);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders(Headers.SIMPLE_HEADER)));

        tableView.addDataLongClickListener(new longClickListenerHandler());

        refreshTable();

    }


    private void initialize() {

        finca = (TextView)findViewById(R.id.finca_text);
        rodal = (TextView)findViewById(R.id.rodal_text);
        viaje = (TextView)findViewById(R.id.viaje_text);

        circunf = (EditText) findViewById(R.id.circunf_text);
        largo = (EditText) findViewById(R.id.largo_text);

        clases = (Spinner)findViewById(R.id.clase_spinner);

        ArrayAdapter<CharSequence> clasesAdapter = ArrayAdapter.createFromResource(this, R.array.clasif_trozas_array, R.layout.support_simple_spinner_dropdown_item);
        clasesAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        clases.setAdapter(clasesAdapter);

        finca.setText(getFinca);
        rodal.setText(getRodal);
        viaje.setText(String.valueOf(getNViaje));

        largo.setText(R.string.default_trozas_value);

        guardar = (Button)findViewById(R.id.guardarTroza_button);
        guardar = (Button)findViewById(R.id.guardarTroza_button);
        verTabla = (Button)findViewById(R.id.verTablaTr_button);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!checkFields()) {

                Troza troza = new Troza();
                troza.setViajeid(viajeid);
                troza.setClasificacion(clases.getSelectedItem().toString());
                troza.setCircunferencia( Double.parseDouble(circunf.getText().toString()));
                troza.setLargo( Double.parseDouble(largo.getText().toString()));

                    if (new DBAdapter(Registro_Trozas_Cosechas.this).guardarTroza(troza.getViajeid(), troza.getClasificacion(), troza.getCircunferencia(), troza.getLargo())) {
                        Toast.makeText(Registro_Trozas_Cosechas.this, "Datos insertados", Toast.LENGTH_SHORT).show();

                        cleanfields();
                        refreshTable();

                    } else {
                        Toast.makeText(Registro_Trozas_Cosechas.this, "Error al guardar!", Toast.LENGTH_SHORT).show();
                    }

                }else
                {
                    Toast.makeText(Registro_Trozas_Cosechas.this, "No pueden haber datos vacios", Toast.LENGTH_SHORT).show();
                }

            }
        });

        verTabla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Registro_Trozas_Cosechas.this, Troza_TablaDetalle.class);
                intent.putExtra("viajeId", viajeid);
                startActivity(intent);
            }
        });

    }

    private boolean checkFields() {

        return circunf.getText().toString().equals("") || largo.getText().toString().equals("");
    }

    private void cleanfields(){

        circunf.setText("");
        largo.setText(R.string.default_trozas_value);
    }

    private void refreshTable(){

        tableView.setDataAdapter(new SimpleTableDataAdapter(Registro_Trozas_Cosechas.this, tableHelper.getTrozasxID(viajeid)));

    }

    private class longClickListenerHandler implements de.codecrafters.tableview.listeners.TableDataLongClickListener<String[]> {

        @Override
        public boolean onDataLongClicked(int rowIndex, String[] clickedData) {
            final int id =  Integer.parseInt(((String[]) clickedData)[6]);

            AlertDialog.Builder alerta = new AlertDialog.Builder(Registro_Trozas_Cosechas.this);
            alerta.setMessage("¿Desea borrar este registro?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            if (new DBAdapter(Registro_Trozas_Cosechas.this).eliminarTroza(id)) {

                                Toast.makeText(Registro_Trozas_Cosechas.this, "Registro Borrado", Toast.LENGTH_SHORT).show();
                                refreshTable();

                            } else {
                                Toast.makeText(Registro_Trozas_Cosechas.this, "El registro no se pudo borrar", Toast.LENGTH_SHORT).show();
                            }

                            dialogInterface.dismiss();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Borrar Troza");
            myAlert.show();

            return true;
        }
    }
}
