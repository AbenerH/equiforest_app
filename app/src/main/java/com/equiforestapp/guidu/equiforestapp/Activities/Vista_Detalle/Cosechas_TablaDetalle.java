package com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Trozas_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class Cosechas_TablaDetalle extends AppCompatActivity implements ITableActivity {

    TextView text_volumen;
    TableView<String[]> tableView;
    Table_Trozas_Helper helper;
    double volumen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_trozas__tabla_detalle);

        DecimalFormat decimalFormat = new DecimalFormat("#.####");

        text_volumen = findViewById(R.id.text_volumenTotal);
        volumen = new DBAdapter(this).getVolumenTotal();
        text_volumen.setText((String.valueOf(decimalFormat.format(volumen))));

        showData();
    }

    @Override
    public void showData() {

        helper = new Table_Trozas_Helper(this);
        tableView = (TableView<String[]>)findViewById(R.id.tableView_AllTrozas);
        tableView.setColumnCount(6);
        tableView.setHeaderBackgroundColor(Color.parseColor("#3d3d3d"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, helper.getHeaders(Headers.DETAIL_HEADER)));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, helper.getAllTrozas()));
    }

}
