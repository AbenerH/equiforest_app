package com.equiforestapp.guidu.equiforestapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Debug;
import android.util.Log;

import com.equiforestapp.guidu.equiforestapp.Modelo.Contenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.DetalleContenedor;
import com.equiforestapp.guidu.equiforestapp.Modelo.PackingList;
import com.equiforestapp.guidu.equiforestapp.Modelo.Rodal;
import com.equiforestapp.guidu.equiforestapp.Modelo.Troza;
import com.equiforestapp.guidu.equiforestapp.Modelo.TrozaCalculation;
import com.equiforestapp.guidu.equiforestapp.Modelo.Ubicaciones;
import com.equiforestapp.guidu.equiforestapp.Modelo.Views.DetalleContenedor_View;
import com.equiforestapp.guidu.equiforestapp.Modelo.Views.Troza_View;
import com.equiforestapp.guidu.equiforestapp.Modelo.Viaje;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DBAdapter {

    private Context context;
    private SQLiteDatabase db;
    private DBHelper helper;

    public DBAdapter(Context context) {
        this.context = context;
        helper = new DBHelper(context);
    }

    private String[] getHeaders(String table) {
        String[] headers = null;

        switch (table) {

            case Constants.TABLE_TROZAS:
                headers = new String[]{Constants.COLUMN_TROZAS_ID, Constants.COLUMN_TROZAS_CLASE, Constants.COLUMN_TROZAS_CIRCUNFERENCIA, Constants.COLUMN_TROZAS_LONGITUD, Constants.COLUMN_TROZAS_VIAJEID};
                break;

            case Constants.TABLE_VIAJES:
                headers = new String[]{Constants.COLUMN_VIAJE_ID, Constants.COLUMN_VIAJE_NUCLEO, Constants.COLUMN_VIAJE_FINCA, Constants.COLUMN_VIAJE_RODAL, Constants.COLUMN_VIAJE_FECHA_INGRESO, Constants.COLUMN_VIAJE_UBICACION, Constants.COLUMN_VIAJE_NVIAJES, Constants.COLUMN_VIAJE_VIAJEID, Constants.COLUMN_VIAJE_NOTROZASC};
                break;

            case Constants.TABLE_RODALES:
                headers = new String[]{Constants.COLUMN_RODALES_ID, Constants.COLUMN_RODALES_FINCA, Constants.COLUMN_RODALES_NUCLEO, Constants.COLUMN_RODALES_RODAL};
                break;

            case Constants.TABLE_UBICACIONES:
                headers = new String[]{Constants.COLUMN_RODALES_ID, Constants.COLUMN_UBICACIONES_NOMBRE};
                break;

            case Constants.VIEW_TROZAS_VIAJES:
                headers = new String[]{Constants.COLUMN_TROZAS_ID, Constants.COLUMN_VIAJE_FINCA, Constants.COLUMN_VIAJE_RODAL, Constants.COLUMN_TROZAS_CLASE, Constants.COLUMN_TROZAS_CIRCUNFERENCIA, Constants.COLUMN_TROZAS_LONGITUD, Constants.COLUMN_TROZAS_ESTADO};
                break;

            case Constants.TABLE_FACTURACONTENEDOR:
                headers = new String[]{Constants.COLUMN_FCONTENEDOR_ID, Constants.COLUMN_FCONTENEDOR_FACTURA_NO, Constants.COLUMN_FCONTENEDOR_PACKINGID, Constants.COLUMN_FCONTENEDOR_CONTENEDOR, Constants.COLUMN_FCONTENEDOR_CABEZAL, Constants.COLUMN_FCONTENEDOR_CHOFER, Constants.COLUMN_FCONTENEDOR_SELLO, Constants.COLUMN_FCONTENEDOR_FECHASALIDA, Constants.COLUMN_FCONTENEDOR_MAXCARGOWGT};
                break;

            case Constants.TABLE_FACTURADETALLECONTENEDOR:
                headers = new String[]{Constants.COLUMN_FDETALLECONTENEDOR_ID, Constants.COLUMN_FDETALLECONTENEDOR_PACKINGLIST, Constants.COLUMN_FDETALLECONTENEDOR_CIRCMEDIA, Constants.COLUMN_FDETALLECONTENEDOR_LONG};
                break;

            case Constants.TABLE_PACKINGLIST:
                headers = new String[]{Constants.COLUMN_PACKINGLIST_ID, Constants.COLUMN_PACKINGLIST_FECHA, Constants.COLUMN_PACKINGLIST_CONSECUTIVO, Constants.COLUMN_PACKINGLIST_CLASE, Constants.COLUMN_PACKINGLIST_PACKINGID, Constants.COLUMN_PACKINGLIST_CONTENEDOR, Constants.COLUMN_PACKINGLIST_UBICACION, Constants.COLUMN_PACKINGLIST_DESCUENTOCRC, Constants.COLUMN_PACKINGLIST_DESCUENTOLONG};
                break;

            case Constants.VIEW_TROZAS_COMERCIALIZACION:
                headers = new String[]{Constants.COLUMN_FDETALLECONTENEDOR_ID, Constants.COLUMN_PACKINGLIST_UBICACION, Constants.COLUMN_PACKINGLIST_FECHA, Constants.COLUMN_PACKINGLIST_CONSECUTIVO, Constants.COLUMN_PACKINGLIST_CLASE, Constants.COLUMN_FDETALLECONTENEDOR_CIRCMEDIA, Constants.COLUMN_FDETALLECONTENEDOR_LONG,};
                break;
        }

        return headers;
    }

    //Guardar Datos

    public boolean guardarTroza(String viajeid, String clase, double circunf, double largo) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_TROZAS_VIAJEID, viajeid);
            cv.put(Constants.COLUMN_TROZAS_CLASE, clase);
            cv.put(Constants.COLUMN_TROZAS_CIRCUNFERENCIA, circunf);
            cv.put(Constants.COLUMN_TROZAS_LONGITUD, largo);
            cv.put(Constants.COLUMN_TROZAS_ESTADO, 0); //Estado = 0

            long result = db.insert(Constants.TABLE_TROZAS, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
            helper.close();
        }
        return false;
    }

    public boolean guardarViaje(Viaje viaje) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_VIAJE_NUCLEO, viaje.getNucleo());
            cv.put(Constants.COLUMN_VIAJE_FINCA, viaje.getFinca());
            cv.put(Constants.COLUMN_VIAJE_RODAL, viaje.getRodal());
            cv.put(Constants.COLUMN_VIAJE_FECHA_INGRESO, viaje.getFechaIngreso());
            cv.put(Constants.COLUMN_VIAJE_UBICACION, viaje.getUbicacion());
            cv.put(Constants.COLUMN_VIAJE_NVIAJES, viaje.getN_viaje());
            cv.put(Constants.COLUMN_VIAJE_VIAJEID, viaje.getViajeid());
            cv.put(Constants.COLUMN_VIAJE_NOTROZASC, viaje.getNoTrozasC());
            cv.put(Constants.COLUMN_VIAJE_ESTADO, 0); //Estado = 0

            long result = db.insert(Constants.TABLE_VIAJES, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
            helper.close();
        }
        return false;
    }

    public boolean guardarRodal(Rodal rodal) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();

            if (rodal.getId() != 0){
                cv.put(Constants.COLUMN_RODALES_ID, rodal.getId());
            }

            cv.put(Constants.COLUMN_RODALES_FINCA, rodal.getFinca());
            cv.put(Constants.COLUMN_RODALES_NUCLEO, rodal.getNucleo());
            cv.put(Constants.COLUMN_RODALES_RODAL, rodal.getRodal());

            long result = db.insert(Constants.TABLE_RODALES, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
            helper.close();
        }
        return false;

    }

    public boolean guardarUbicacion(Ubicaciones ubicaciones) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_UBICACIONES_NOMBRE, ubicaciones.getNombre());

            long result = db.insert(Constants.TABLE_UBICACIONES, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
            helper.close();
        }
        return false;
    }

    public boolean guardarContenedor(Contenedor contenedor) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FCONTENEDOR_CONTENEDOR, contenedor.getContenedorId());
            cv.put(Constants.COLUMN_FCONTENEDOR_CABEZAL, contenedor.getCabezal());
            cv.put(Constants.COLUMN_FCONTENEDOR_CHOFER, contenedor.getChofer());
            cv.put(Constants.COLUMN_FCONTENEDOR_SELLO, contenedor.getSello());
            cv.put(Constants.COLUMN_FCONTENEDOR_FECHASALIDA, contenedor.getFechaSalida());
            cv.put(Constants.COLUMN_FCONTENEDOR_MAXCARGOWGT, contenedor.getMaxCargoWgt());
            cv.put(Constants.COLUMN_FCONTENEDOR_ESTADO, 0); //Estado = 0

            long result = db.insert(Constants.TABLE_FACTURACONTENEDOR, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            helper.close();
        }
        return false;

    }

    public boolean guardarDetalleContenedor(DetalleContenedor detalleContenedor) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FDETALLECONTENEDOR_CIRCMEDIA, detalleContenedor.getCircunferencia());
            cv.put(Constants.COLUMN_FDETALLECONTENEDOR_LONG, detalleContenedor.getLongitud());
            cv.put(Constants.COLUMN_FDETALLECONTENEDOR_PACKINGLIST, detalleContenedor.getPackingListId());
            cv.put(Constants.COLUMN_FDETALLECONTENEDOR_ESTADO, 0);

            long result = db.insert(Constants.TABLE_FACTURADETALLECONTENEDOR, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            helper.close();
        }
        return false;
    }

    public boolean guardarPackingList(PackingList packingList){

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_PACKINGLIST_FECHA, packingList.getFecha());
            cv.put(Constants.COLUMN_PACKINGLIST_CONSECUTIVO, packingList.getConsecutivo());
            cv.put(Constants.COLUMN_PACKINGLIST_CLASE, packingList.getClase());
            cv.put(Constants.COLUMN_PACKINGLIST_PACKINGID, packingList.getPackingListId());
            cv.put(Constants.COLUMN_PACKINGLIST_DESCUENTOCRC, packingList.getDescuentoCrc());
            cv.put(Constants.COLUMN_PACKINGLIST_DESCUENTOLONG, packingList.getDescuentoLong());
            cv.put(Constants.COLUMN_PACKINGLIST_UBICACION, packingList.getUbicacion());
            cv.put(Constants.COLUMN_PACKINGLIST_ESTADO, 0);

            long result = db.insert(Constants.TABLE_PACKINGLIST, null, cv);
            cv.clear();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            helper.close();
        }
        return false;
    }

    //Recuperar Datos
    public ArrayList<Rodal> recuperarListaRodales() {

        ArrayList<Rodal> listado = new ArrayList<>();
        String[] columnas;

        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.TABLE_RODALES);

        cursor = db.query(Constants.TABLE_RODALES, columnas, null, null, null, null, Constants.COLUMN_RODALES_RODAL);

        Rodal rodal;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            rodal = cursorToRodal(cursor);
            listado.add(rodal);
            cursor.moveToNext();
        }

        cursor.close();

        return listado;
    }

    //Recuperar Datos
    public ArrayList<Ubicaciones> recuperarListaUbicaciones() {

        ArrayList<Ubicaciones> listado = new ArrayList<>();
        String[] columnas;

        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.TABLE_UBICACIONES);

        cursor = db.query(Constants.TABLE_UBICACIONES, columnas, null, null, null, null, null);

        Ubicaciones ubicaciones;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ubicaciones = cursorToUbicacion(cursor);
            listado.add(ubicaciones);
            cursor.moveToNext();
        }

        cursor.close();

        return listado;
    }

    public ArrayList<Troza> recuperarListaTrozas(int opcion) {

        //Devuelve todas las trozas --No parametros

        ArrayList<Troza> listado = new ArrayList<>();
        String[] columnas;
        String whereClause = Constants.COLUMN_TROZAS_ESTADO + "=?";
        String[] whereArgs = {"0"};

        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.TABLE_TROZAS);

        cursor = db.query(Constants.TABLE_TROZAS, columnas, whereClause, whereArgs, null, null, null);

        Troza troza;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            if (opcion == 1) {
                troza = cursorToTroza(cursor);
            } else {
                troza = cursorToTrozaAPI(cursor);
            }
            listado.add(troza);
            cursor.moveToNext();
        }
        cursor.close();

        return listado;
    }

    public ArrayList<Troza_View> recuperarVistaTrozasViajes() {

        ArrayList<Troza_View> listado = new ArrayList<>();
        String[] columnas;
        String whereClause = Constants.COLUMN_TROZAS_ESTADO + "=?";
        String[] whereArgs = {"0"};


        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.VIEW_TROZAS_VIAJES);

        cursor = db.query(Constants.VIEW_TROZAS_VIAJES, columnas, whereClause, whereArgs, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Troza_View troza = cursorToTrozaViaje(cursor);
            listado.add(troza);
            cursor.moveToNext();
        }

        cursor.close();

        return listado;
    }

    public ArrayList<Viaje> recuperarListaViajes(int opcion) {

        ArrayList<Viaje> listado = new ArrayList<>();

        String[] columnas = getHeaders(Constants.TABLE_VIAJES);

        //Busqueda Normal
        if (opcion == 1) {

            try {
                db = helper.getWritableDatabase();
                Cursor cursor = db.query(Constants.TABLE_VIAJES, columnas, null, null, null, null, null);

                Viaje viaje;

                if (context != null) {
                    while (cursor.moveToNext()) {

                        viaje = cursorToViaje(cursor);
                        listado.add(viaje);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {

            String whereClause = Constants.COLUMN_VIAJE_ESTADO + "=?";
            String[] whereArgs = {"0"};

            try {
                db = helper.getWritableDatabase();
                Cursor cursor = db.query(Constants.TABLE_VIAJES, columnas, whereClause, whereArgs, null, null, null);

                Viaje viaje;

                if (context != null) {

                    while (cursor.moveToNext()) {

                        viaje = cursorToViajeAPI(cursor);

                        listado.add(viaje);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return listado;
    }

    public ArrayList<Troza> recuperarListaTrozasxViaje(String id) {

        ArrayList<Troza> lista = new ArrayList<>();

        db = helper.getReadableDatabase();
        Cursor cursor = db.query(Constants.TABLE_TROZAS, getHeaders(Constants.TABLE_TROZAS), Constants.COLUMN_TROZAS_VIAJEID + " = ?", new String[]{id}, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Troza troza = cursorToTroza(cursor);
            lista.add(troza);
            cursor.moveToNext();
        }

        cursor.close();
        return lista;
    }


    public ArrayList<Contenedor> recuperarListaContenedores(int opcion) {

        ArrayList<Contenedor> listado = new ArrayList<>();

        String[] columnas = getHeaders(Constants.TABLE_FACTURACONTENEDOR);

        //Busqueda Normal
        if (opcion == 1) {

            try {
                db = helper.getWritableDatabase();
                Cursor cursor = db.query(Constants.TABLE_FACTURACONTENEDOR, columnas, null, null, null, null, null);

                Contenedor contenedor;

                if (context != null) {
                    while (cursor.moveToNext()) {

                        contenedor = cursorToContenedor(cursor);
                        listado.add(contenedor);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {

            String whereClause = Constants.COLUMN_FCONTENEDOR_ESTADO + "=?";
            String[] whereArgs = {"0"};

            try {
                db = helper.getWritableDatabase();
                Cursor cursor = db.query(Constants.TABLE_FACTURACONTENEDOR, columnas, whereClause, whereArgs, null, null, null);

                Contenedor contenedor;

                if (context != null) {

                    while (cursor.moveToNext()) {

                        contenedor = cursorToContenedorAPI(cursor);
                        listado.add(contenedor);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return listado;
    }

/*    public ArrayList<DetalleContenedor> recuperarListaDetalleContenedoresxContenedor(String nContenedor) {

        ArrayList<DetalleContenedor> listado = new ArrayList<>();

        db = helper.getReadableDatabase();
        Cursor cursor = db.query(Constants.TABLE_FACTURADETALLECONTENEDOR, getHeaders(Constants.TABLE_FACTURADETALLECONTENEDOR), Constants.COLUMN_FDETALLECONTENEDOR_PACKINGLIST + " = ?", new String[]{nContenedor}, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DetalleContenedor detalleContenedor = cursorToDetalleContenedor(cursor);
            listado.add(detalleContenedor);
            cursor.moveToNext();
        }

        cursor.close();

        return listado;
    }*/

    public ArrayList<DetalleContenedor> recuperarListaDetalleContenedores(int opcion) {

        ArrayList<DetalleContenedor> listado = new ArrayList<>();
        String[] columnas = getHeaders(Constants.TABLE_FACTURADETALLECONTENEDOR);

        //Busqueda normal

        if (opcion == 1) {

            try {

                db = helper.getReadableDatabase();
                Cursor cursor = db.query(Constants.TABLE_FACTURADETALLECONTENEDOR, columnas, null, null, null, null, null);

                DetalleContenedor detalleContenedor;

                if (context != null) {
                    while (cursor.moveToNext()) {

                        detalleContenedor = cursorToDetalleContenedor(cursor);
                        listado.add(detalleContenedor);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                Log.e("Error", e.getMessage());
            }

        } else {

            String whereClause = Constants.COLUMN_FDETALLECONTENEDOR_ESTADO + "=?";
            String[] whereArgs = {"0"};

            try {

                db = helper.getWritableDatabase();
                Cursor cursor = db.query(Constants.TABLE_FACTURADETALLECONTENEDOR, columnas, whereClause, whereArgs, null, null, null);

                DetalleContenedor detalleContenedor;

                if (context != null) {

                    while (cursor.moveToNext()) {

                        detalleContenedor = cursorToDetalleContenedorAPI(cursor);
                        listado.add(detalleContenedor);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return listado;
    }

    public ArrayList<PackingList> recuperarListaPackingList(int opcion) {

        ArrayList<PackingList> listado = new ArrayList<>();
        String[] columnas = getHeaders(Constants.TABLE_PACKINGLIST);

        //Busqueda normal

        if (opcion == 1) {

            try {

                db = helper.getReadableDatabase();
                Cursor cursor = db.query(Constants.TABLE_PACKINGLIST, columnas, null, null, null, null,  null);

                PackingList packingList;

                if (context != null) {
                    while (cursor.moveToNext()) {

                        packingList = cursorToPackingList(cursor);
                        listado.add(packingList);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                Log.e("Error", e.getMessage());
            }

        } else {

            String whereClause = Constants.COLUMN_PACKINGLIST_ESTADO + "=?";
            String[] whereArgs = {"0"};

            try {

                db = helper.getWritableDatabase();
                Cursor cursor = db.query(Constants.TABLE_PACKINGLIST, columnas, whereClause, whereArgs, null, null, null);

                PackingList packingList;

                if (context != null) {

                    while (cursor.moveToNext()) {

                        packingList = cursorToPackingListAPI(cursor);
                        listado.add(packingList);
                    }
                }

                cursor.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return listado;

    }

    public ArrayList<PackingList> recuperarListaPackingListDisponibles() {

        ArrayList<PackingList> listado = new ArrayList<>();
        String[] columnas = getHeaders(Constants.TABLE_PACKINGLIST);

        try {

            db = helper.getReadableDatabase();
            Cursor cursor = db.query(Constants.TABLE_PACKINGLIST, columnas,  Constants.COLUMN_PACKINGLIST_CONTENEDOR + " is null or " + Constants.COLUMN_PACKINGLIST_CONTENEDOR + " = ?", new String[]{""} , null, null, null);

            PackingList packingList;

            if (context != null) {
                while (cursor.moveToNext()) {

                    packingList = cursorToPackingList(cursor);
                    listado.add(packingList);
                }
            }

            cursor.close();

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        }

        return listado;
    }

    public ArrayList<DetalleContenedor_View> recuperarVistaTrozasComercializacion() {

        ArrayList<DetalleContenedor_View> listado = new ArrayList<>();
        String[] columnas;
        String order = Constants.COLUMN_PACKINGLIST_UBICACION + ", " + Constants.COLUMN_PACKINGLIST_CONSECUTIVO + ", " + Constants.COLUMN_PACKINGLIST_CLASE;

        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.VIEW_TROZAS_COMERCIALIZACION);

        cursor = db.query(Constants.VIEW_TROZAS_COMERCIALIZACION, columnas, "estado = ?", new String[]{"0"}, null, null, order);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            DetalleContenedor_View detalle = cursorToDetalleContenedor_View(cursor);
            listado.add(detalle);
            cursor.moveToNext();
        }

        cursor.close();

        return listado;
    }

    public boolean checkContenedor(String contenedor){

        String[] columnas = {Constants.COLUMN_FCONTENEDOR_CONTENEDOR};

        String whereClause = Constants.COLUMN_FCONTENEDOR_CONTENEDOR + "= ?";
        String[] whereArgs = {String.valueOf(contenedor)};
        try {

            db = helper.getWritableDatabase();
            Cursor cursor = db.query(Constants.TABLE_FACTURACONTENEDOR, columnas, whereClause, whereArgs, null, null, null);


            if (context != null) {

                if (cursor.moveToNext()) {
                    cursor.close();
                    return true;
                }

            }

        } catch (SQLException exc) {
            Log.e("Error", exc.getMessage());
        }

        return false;

    }

    public boolean checkPackingList(String packingID){

        String[] columnas = {Constants.COLUMN_PACKINGLIST_PACKINGID };

        String whereClause = Constants.COLUMN_PACKINGLIST_PACKINGID + "= ?";
        String[] whereArgs = {String.valueOf(packingID)};
        try {

            db = helper.getWritableDatabase();
            Cursor cursor = db.query(Constants.TABLE_PACKINGLIST, columnas, whereClause, whereArgs, null, null, null);

            if (context != null) {

                if (cursor.moveToNext()) {
                    cursor.close();
                    return true;
                }

            }

        } catch (SQLException exc) {
            Log.e("Error", exc.getMessage());
        }

        return false;
    }

    public boolean getViajeId(String viajeid) {

        String[] columnas = {Constants.COLUMN_VIAJE_VIAJEID};

        String whereClause = Constants.COLUMN_VIAJE_VIAJEID + "= ?";
        String[] whereArgs = {String.valueOf(viajeid)};
        try {

            db = helper.getWritableDatabase();
            Cursor cursor = db.query(Constants.TABLE_VIAJES, columnas, whereClause, whereArgs, null, null, null);


            if (context != null) {

                if (cursor.moveToNext()) {
                    cursor.close();
                    return true;
                }

            }

        } catch (SQLException exc) {
            Log.e("Error", exc.getMessage());
        }

        return false;
    }


    //Convertir de cursor a objeto

    private DetalleContenedor cursorToDetalleContenedor(Cursor cursor) {

        DetalleContenedor detalleContenedor;

        int id = cursor.getInt(0);
        String nContenedor = cursor.getString(1);
        double circunf = cursor.getDouble(2);
        double largo = cursor.getDouble(3);

        detalleContenedor = new DetalleContenedor(id, nContenedor, circunf, largo);
        return detalleContenedor;

    }

    private DetalleContenedor cursorToDetalleContenedorAPI(Cursor cursor) {

        DetalleContenedor detalleContenedor;

        int id = 0;
        String nContenedor = cursor.getString(1);
        double circunf = cursor.getDouble(2);
        double largo = cursor.getDouble(3);

        detalleContenedor = new DetalleContenedor(id, nContenedor, circunf, largo);
        return detalleContenedor;

    }

    private Troza cursorToTroza(Cursor cursor) {

        Troza troza;

        int id = cursor.getInt(0);
        String clase = cursor.getString(1);
        double circunferencia = cursor.getDouble(2);
        double largo = cursor.getDouble(3);
        String viajeid = cursor.getString(4);

        troza = new Troza();

        troza.setId(id);

        troza.setClasificacion(clase);
        troza.setCircunferencia(circunferencia);
        troza.setLargo(largo);
        troza.setViajeid(viajeid);

        return troza;

    }

    private Troza cursorToTrozaAPI(Cursor cursor) {

        Troza troza;

        int id = 0;
        String clase = cursor.getString(1);
        double circunferencia = cursor.getDouble(2);
        double largo = cursor.getDouble(3);
        String viajeid = cursor.getString(4);

        troza = new Troza();

        troza.setId(id);
        troza.setClasificacion(clase);
        troza.setCircunferencia(circunferencia);
        troza.setLargo(largo);
        troza.setViajeid(viajeid);

        return troza;

    }

    private Troza_View cursorToTrozaViaje(Cursor cursor) {

        Troza_View troza_view;

        int id = cursor.getInt(0);
        String finca = cursor.getString(1);
        String rodal = cursor.getString(2);
        String clase = cursor.getString(3);
        double circunferencia = cursor.getDouble(4);
        double largo = cursor.getDouble(5);

        troza_view = new Troza_View();

        troza_view.setId(id);
        troza_view.setFinca(finca);
        troza_view.setRodal(rodal);
        troza_view.setClasificacion(clase);
        troza_view.setCircunferencia(circunferencia);
        troza_view.setLargo(largo);

        return troza_view;

    }

    private DetalleContenedor_View cursorToDetalleContenedor_View(Cursor cursor) {

        DetalleContenedor_View detalleContenedorView;

        int id = cursor.getInt(0);
        String ubicacion = cursor.getString(1);
        String fecha = cursor.getString(2);
        int consecutivo = cursor.getInt(3);
        String clase = cursor.getString(4);
        double circunferencia = cursor.getDouble(5);
        double largo = cursor.getDouble(6);

        detalleContenedorView = new DetalleContenedor_View();

        detalleContenedorView.setId(id);
        detalleContenedorView.setConsecutivo(consecutivo);
        detalleContenedorView.setUbicacion(ubicacion);
        detalleContenedorView.setFecha(fecha);
        detalleContenedorView.setClase(clase);
        detalleContenedorView.setCircunferencia(circunferencia);
        detalleContenedorView.setLongitud(largo);

        return detalleContenedorView;
    }

    private Viaje cursorToViajeAPI(Cursor cursor) {

        Viaje viaje;

        int id = 0;
        String nucleo = cursor.getString(1);
        String finca = cursor.getString(2);
        String rodal = cursor.getString(3);
        String fecha_ingreso = cursor.getString(4);
        String ubicacion = cursor.getString(5);
        int n_viaje = cursor.getInt(6);
        String viajeid = cursor.getString(7);
        int noTrozasC = cursor.getInt(8);

        viaje = new Viaje(id, nucleo, finca, rodal, fecha_ingreso, ubicacion, viajeid, n_viaje, noTrozasC);

        return viaje;

    }

    private Viaje cursorToViaje(Cursor cursor) {

        Viaje viaje;

        int id = cursor.getInt(0);
        String nucleo = cursor.getString(1);
        String finca = cursor.getString(2);
        String rodal = cursor.getString(3);
        String fecha_ingreso = cursor.getString(4);
        String ubicacion = cursor.getString(5);
        int n_viaje = cursor.getInt(6);
        String viajeid = cursor.getString(7);
        int noTrozasC = cursor.getInt(8);

        viaje = new Viaje(id, nucleo, finca, rodal, fecha_ingreso, ubicacion, viajeid, n_viaje, noTrozasC);

        return viaje;

    }

    private Rodal cursorToRodal(Cursor cursor) {

        Rodal rodal;

        int id = cursor.getInt(0);
        String finca = cursor.getString(1);
        String nucleo = cursor.getString(2);
        String numeroRodal = cursor.getString(3);

        rodal = new Rodal(id, finca, nucleo, numeroRodal);

        return rodal;
    }

    private Ubicaciones cursorToUbicacion(Cursor cursor) {

        Ubicaciones ubicaciones;

        int id = cursor.getInt(0);
        String nombre = cursor.getString(1);

        ubicaciones = new Ubicaciones(id, nombre);

        return ubicaciones;

    }

    private Contenedor cursorToContenedor(Cursor cursor) {

        Contenedor contenedor;

        int id = cursor.getInt(0);
        String facturaNo = cursor.getString(1);
        String packingId = cursor.getString(2);
        String nContenedor = cursor.getString(3);
        String cabezal = cursor.getString(4);
        String chofer = cursor.getString(5);
        String sello = cursor.getString(6);
        String fechaSalida = cursor.getString(7);
        double maxWeight = cursor.getDouble(8);

        contenedor = new Contenedor(id, facturaNo, packingId, nContenedor, cabezal, chofer, sello, fechaSalida, maxWeight);
        return contenedor;
    }

    private Contenedor cursorToContenedorAPI(Cursor cursor) {

        Contenedor contenedor;

        int id = 0;
        String facturaNo = cursor.getString(1);
        String packingId = cursor.getString(2);
        String nContenedor = cursor.getString(3);
        String cabezal = cursor.getString(4);
        String chofer = cursor.getString(5);
        String sello = cursor.getString(6);
        String fechaSalida = cursor.getString(7);
        double maxWeight = cursor.getDouble(8);

        contenedor = new Contenedor(id, facturaNo, packingId, nContenedor, cabezal, chofer, sello, fechaSalida, maxWeight);
        return contenedor;
    }

    private PackingList cursorToPackingList(Cursor cursor) {

        PackingList packingList;

        int id = cursor.getInt(0);
        String fecha = cursor.getString(1);
        int consecutivo = cursor.getInt(2);
        String clase = cursor.getString(3);
        String packingId = cursor.getString(4);
        String contenedor = cursor.getString(5);
        String ubicacion = cursor.getString(6);
        double descuentoCrc = cursor.getDouble(7);
        double descuentoLong = cursor.getDouble(8);

        packingList = new PackingList(id, fecha, consecutivo, clase, packingId, contenedor, ubicacion, descuentoCrc, descuentoLong);
        return packingList;
    }

    private PackingList cursorToPackingListAPI(Cursor cursor) {

        PackingList packingList;

        int id = 0;
        String fecha = cursor.getString(1);
        int consecutivo = cursor.getInt(2);
        String clase = cursor.getString(3);
        String packingId = cursor.getString(4);
        String contenedor = cursor.getString(5);
        String ubicacion = cursor.getString(6);
        double descuentoCrc = cursor.getDouble(7);
        double descuentoLong = cursor.getDouble(8);

        packingList = new PackingList(id, fecha, consecutivo, clase, packingId, contenedor, ubicacion, descuentoCrc, descuentoLong);
        return packingList;

    }

/*    private DetalleContenedor_View cursorToDetalleContenedor_View(Cursor cursor) {

        DetalleContenedor_View detalleView;

        int id = cursor.getInt(0);
        String facturaNo = cursor.getString(1);
        String packingList = cursor.getString(2);
        double circunf = cursor.getDouble(3);
        double longitud = cursor.getDouble(4);

        detalleView = new DetalleContenedor_View();

        detalleView.setId(id);
        detalleView.setFacturaNo(facturaNo);
        detalleView.setPackingListId(packingList);
        detalleView.setCircunferencia(circunf);
        detalleView.setLongitud(longitud);

        return detalleView;

    }*/

    //Actualizar Datos

    public boolean updateContenedor(Contenedor contenedor){

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FCONTENEDOR_CABEZAL, contenedor.getCabezal());
            cv.put(Constants.COLUMN_FCONTENEDOR_CHOFER, contenedor.getChofer());
            cv.put(Constants.COLUMN_FCONTENEDOR_SELLO, contenedor.getSello());
            cv.put(Constants.COLUMN_FCONTENEDOR_FECHASALIDA, contenedor.getFechaSalida());
            cv.put(Constants.COLUMN_FCONTENEDOR_MAXCARGOWGT, contenedor.getMaxCargoWgt());

            int result = db.update(Constants.TABLE_FACTURACONTENEDOR, cv, Constants.COLUMN_FCONTENEDOR_CONTENEDOR + " = ?", new String[]{String.valueOf(contenedor.getContenedorId())});

            if (result != 0) {
                Log.d("Debugging", "El contenedor se ha actualizado");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }


        return false;
    }

    public boolean updatePackingListContenedor(String id, String contenedor) {

        if(checkPackingListContainer(contenedor)){

            Log.d("Debugging", "Packing List ya asignado actualizandose");
            borrarPackingListContainer(contenedor);
        }

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_PACKINGLIST_CONTENEDOR, contenedor);

            Log.d("Debugging", "Contenedor Value: " + contenedor);

            int result = db.update(Constants.TABLE_PACKINGLIST, cv, Constants.COLUMN_PACKINGLIST_PACKINGID + " = ?", new String[]{id});

            if (result != 0) {
                Log.d("Debugging", "El PackingList se ha actualizado");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return false;

    }

    public boolean updateContenedorPackingList(String id, String contenedor) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FCONTENEDOR_PACKINGID, id);

            int result = db.update(Constants.TABLE_FACTURACONTENEDOR, cv,  Constants.COLUMN_PACKINGLIST_CONTENEDOR + " = ?", new String[]{contenedor});

            if (result != 0) {
                Log.d("Debugging", "El PackingList se ha actualizado");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;

    }

    private void borrarPackingListContainer(String contenedor) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            //cv.put(Constants.COLUMN_PACKINGLIST_CONTENEDOR, "");
            cv.putNull(Constants.COLUMN_PACKINGLIST_CONTENEDOR);

            int result = db.update(Constants.TABLE_PACKINGLIST, cv,  Constants.COLUMN_PACKINGLIST_CONTENEDOR + " = ?", new String[]{String.valueOf(contenedor)});

            if (result != 0) {
                Log.d("Debugging", "El Contenedor de packing list se ha actualizado");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean checkPackingListContainer(String contenedor) {

        String[] columnas = {Constants.COLUMN_PACKINGLIST_CONTENEDOR};

        String whereClause = Constants.COLUMN_PACKINGLIST_CONTENEDOR + "= ?";
        String[] whereArgs = {String.valueOf(contenedor)};
        try {

            db = helper.getWritableDatabase();
            Cursor cursor = db.query(Constants.TABLE_PACKINGLIST, columnas, whereClause, whereArgs, null, null, null);


            if (context != null) {

                if (cursor.moveToNext()) {
                    cursor.close();
                    return true;
                }

            }

        } catch (SQLException exc) {
            Log.e("Error", exc.getMessage());
        }

        return false;
    }

    public boolean checkContainerPackingList(String packingListId)
    {
        String[] columnas = {Constants.COLUMN_FCONTENEDOR_PACKINGID};

        String whereClause = Constants.COLUMN_FCONTENEDOR_PACKINGID + "= ?";
        String[] whereArgs = {String.valueOf(packingListId)};

        try {

            db = helper.getWritableDatabase();
            Cursor cursor = db.query(Constants.TABLE_PACKINGLIST, columnas, whereClause, whereArgs, null, null, null);

            if (context != null) {

                if (cursor.moveToNext()) {
                    cursor.close();
                    return true;
                }

            }

        } catch (SQLException exc) {
            Log.e("Error", exc.getMessage());
        }

        return false;
    }

    public boolean updateViajesEstado(int opcion) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_VIAJE_ESTADO, opcion);

            int result = db.update(Constants.TABLE_VIAJES, cv, null, null);

            if (result != 0) {
                Log.d("Debugging", "Los viajes subidos ahora estan en OCULTO");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return false;
    }

    public boolean updateTrozasEstado(int opcion) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_TROZAS_ESTADO, opcion);

            int result = db.update(Constants.TABLE_TROZAS, cv, null, null);

            if (result != 0) {
                Log.d("Debugging", "Las trozas subidas ahora estan en OCULTO");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return false;
    }

    public boolean updateDetalleContenedoresEstado(int opcion) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FDETALLECONTENEDOR_ESTADO, opcion);

            int result = db.update(Constants.TABLE_FACTURADETALLECONTENEDOR, cv, null, null);

            if (result != 0) {
                Log.d("Debugging", "Los Detalle Contenedores subidos ahora estan en OCULTO");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return false;

    }

    private void updateAllContenedoresEstado() {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FCONTENEDOR_ESTADO, 0);

            int result = db.update(Constants.TABLE_FACTURACONTENEDOR, cv, null, null);

            if (result != 0) {
                Log.d("Debugging", "Los Detalle Contenedores subidos ahora estan en OCULTO");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public boolean updateContenedoresEstado(ArrayList<Contenedor> contenedores) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_FCONTENEDOR_ESTADO, 1);

            int result = 0;

            for (Contenedor container : contenedores)
                result = db.update(Constants.TABLE_FACTURACONTENEDOR, cv,  Constants.COLUMN_FCONTENEDOR_CONTENEDOR + " = ?", new String[]{String.valueOf(container.getContenedorId())});

            if (result != 0) {
                Log.d("Debugging", "Los Contenedores subidos ahora estan en OCULTO");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return false;
    }

    //Eliminar Datos
    public boolean eliminarViaje(String viajeId) {

        try {
            db = helper.getWritableDatabase();

            if (recuperarListaTrozasxViaje(viajeId).size() > 0) {

                db.delete(Constants.TABLE_TROZAS, "viajeid = ?", new String[]{String.valueOf(viajeId)});

            }

            int result = db.delete(Constants.TABLE_VIAJES, "viajeid = ?", new String[]{String.valueOf(viajeId)});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return false;
    }

    public boolean eliminarContenedor(String contenedorId) {

        try {
            db = helper.getWritableDatabase();

            PackingList packingList;
            packingList = getContainerPackingList(contenedorId);

            if (packingList != null){

                borrarPackingListContainer(contenedorId);
                Log.d("Debugging", "Update contenedor PL eliminado");

            }

            int result = db.delete(Constants.TABLE_FACTURACONTENEDOR, Constants.COLUMN_FCONTENEDOR_CONTENEDOR + " = ?", new String[]{contenedorId});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            db.close();
        }
        return false;
    }


    public boolean eliminarDetalleContenedor(int id) {

        try {
            db = helper.getWritableDatabase();
            int result = db.delete(Constants.TABLE_FACTURADETALLECONTENEDOR, "id = ?", new String[]{String.valueOf(id)});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            db.close();
        }
        return false;
    }

    public boolean eliminarTroza(int id) {

        try {
            db = helper.getWritableDatabase();
            int result = db.delete(Constants.TABLE_TROZAS, "id = ?", new String[]{String.valueOf(id)});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            db.close();
        }
        return false;
    }

    public boolean eliminarUbicacion(int id) {

        try {
            db = helper.getWritableDatabase();
            int result = db.delete(Constants.TABLE_UBICACIONES, "id = ?", new String[]{String.valueOf(id)});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            db.close();
        }
        return false;

    }

    public boolean eliminarRodal(int id) {

        try {
            db = helper.getWritableDatabase();
            int result = db.delete(Constants.TABLE_RODALES, "id = ?", new String[]{String.valueOf(id)});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            db.close();
        }
        return false;
    }

    public boolean eliminarPackingList(String packingid) {

        try {
            db = helper.getWritableDatabase();

            //Delete All trozas from this Packing List
            if (recuperarListaDetalleContenedoresxPackingList(packingid).size() > 0) {

                db.delete(Constants.TABLE_FACTURADETALLECONTENEDOR, Constants.COLUMN_FDETALLECONTENEDOR_PACKINGLIST + " = ?", new String[]{String.valueOf(packingid)});

            }

            //Delete from Contenedor this Packing List
            if(checkContainerPackingList(packingid)){

                try {
                    ContentValues cv = new ContentValues();
                    //cv.put(Constants.COLUMN_PACKINGLIST_CONTENEDOR, "");
                    cv.putNull(Constants.COLUMN_FCONTENEDOR_PACKINGID);

                    int result = db.update(Constants.TABLE_FACTURACONTENEDOR, cv,  Constants.COLUMN_FCONTENEDOR_PACKINGID + " = ?", new String[]{String.valueOf(packingid)});

                    if (result != 0) {
                        Log.d("Debugging", "El Contenedor de packing list se ha actualizado");
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            int result = db.delete(Constants.TABLE_PACKINGLIST, Constants.COLUMN_FDETALLECONTENEDOR_PACKINGLIST + " = ?", new String[]{String.valueOf(packingid)});

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return false;
    }

    public PackingList recuperarPackingListxID(String packingId) {

        PackingList packingList;
        String[] columnas;

        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.TABLE_PACKINGLIST);

        cursor = db.query(Constants.TABLE_PACKINGLIST, columnas, Constants.COLUMN_PACKINGLIST_PACKINGID + " = ?", new String[]{packingId}, null, null, null);

        if (cursor.getCount() == 0) {
            return null;
        } else {

            cursor.moveToFirst();
            packingList = cursorToPackingList(cursor);

            cursor.close();
            return packingList;
        }

    }

    public ArrayList<DetalleContenedor> recuperarListaDetalleContenedoresxPackingList(String packingid) {

        ArrayList<DetalleContenedor> lista = new ArrayList<>();

        db = helper.getReadableDatabase();
        Cursor cursor = db.query(Constants.TABLE_FACTURADETALLECONTENEDOR, getHeaders(Constants.TABLE_FACTURADETALLECONTENEDOR), Constants.COLUMN_FDETALLECONTENEDOR_PACKINGLIST + " = ?", new String[]{packingid}, null, null, null);

        DetalleContenedor detalleContenedor;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            detalleContenedor = cursorToDetalleContenedor(cursor);
            lista.add(detalleContenedor);
            cursor.moveToNext();
        }

        cursor.close();
        return lista;
    }

    public boolean eliminarRodales() {

        try {
            db = helper.getWritableDatabase();
            int result = db.delete(Constants.TABLE_RODALES, null, null);

            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            db.close();
        }
        return false;

    }


    public PackingList getContainerPackingList(String contenedor) {

        PackingList packingList;
        String[] columnas;

        db = helper.getReadableDatabase();
        Cursor cursor;

        columnas = getHeaders(Constants.TABLE_PACKINGLIST);

        cursor = db.query(Constants.TABLE_PACKINGLIST, columnas,  Constants.COLUMN_PACKINGLIST_CONTENEDOR + " = ?", new String[]{contenedor}, null, null, null);

            if (cursor.getCount() == 0){
                return null;
            }
            else {

                cursor.moveToFirst();
                packingList = cursorToPackingList(cursor);

                cursor.close();
                return packingList;
            }

    }


    public boolean checkContenedorSeal(String sello) {

        String[] columnas = {Constants.COLUMN_FCONTENEDOR_SELLO};

        String whereClause = Constants.COLUMN_FCONTENEDOR_SELLO + "= ?";
        String[] whereArgs = {String.valueOf(sello)};
        try {

            db = helper.getWritableDatabase();
            Cursor cursor = db.query(Constants.TABLE_FACTURACONTENEDOR, columnas, whereClause, whereArgs, null, null, null);


            if (context != null) {

                if (cursor.moveToNext()) {
                    cursor.close();
                    return true;
                }

            }

            cursor.close();

        } catch (SQLException exc) {
            Log.e("Error", exc.getMessage());
        }

        return false;

    }

    public Boolean updatePackingListEstado(int opcion) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(Constants.COLUMN_PACKINGLIST_ESTADO, opcion);

            int result = db.update(Constants.TABLE_PACKINGLIST, cv, null, null);

            if (result != 0) {
                Log.d("Debugging", "Los PackingLists subidos ahora estan en OCULTO");
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return false;
    }

    public boolean restartAllFromDb()
    {

        try {

            updatePackingListEstado(0);
            updateDetalleContenedoresEstado(0);
            updateAllContenedoresEstado();
            updateTrozasEstado(0);
            updateViajesEstado(0);

            Log.d("Debugging", "Todos los estados deberian haber sido reiniciados ");

            return true;

        }catch (SQLException e)
        {
            Log.e("Database_error", "Error al actualizar los estados: " + e.getMessage());
        }

        return false;
    }

    public boolean deleteAllFromDb()
    {
        try {

            db = helper.getWritableDatabase();
            db.delete(Constants.TABLE_VIAJES, null, null);
            db.delete(Constants.TABLE_TROZAS, null, null);
            db.delete(Constants.TABLE_PACKINGLIST, null, null);
            db.delete(Constants.TABLE_FACTURADETALLECONTENEDOR, null, null);
            db.delete(Constants.TABLE_FACTURACONTENEDOR, null, null);

            Log.d("Debugging", "Todos los datos deberian haber sido borrados");

            db.close();
            return true;

        }catch (SQLException e)
        {
            Log.e("Database_error", "Error al eliminar los datos: " + e.getMessage());
        }

        db.close();
        return false;
    }

    public boolean guardarRodales(ArrayList<Rodal> rodals) {

        try {
            db = helper.getWritableDatabase();
            ContentValues cv;

            for (Rodal rodal :
                    rodals) {

                cv = new ContentValues();

                cv.put(Constants.COLUMN_RODALES_ID, rodal.getId());
                cv.put(Constants.COLUMN_RODALES_FINCA, rodal.getFinca());
                cv.put(Constants.COLUMN_RODALES_NUCLEO, rodal.getNucleo());
                cv.put(Constants.COLUMN_RODALES_RODAL, rodal.getRodal());

                long result = db.insert(Constants.TABLE_RODALES, null, cv);
                cv.clear();

                if (result == 0) {
                    Log.d("Debugging", "Error at adding rodales");
                    return false;
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
            helper.close();
        }
        return true;

    }

    public double getVolumenTotalxViaje(String viajeId) {

        ArrayList<Troza> listado = recuperarListaTrozasxViaje(viajeId);
        double volumen = 0.0;

        for (int i = 0; i < listado.size(); i++) {

            double result = TrozaCalculation.getSmalian(listado.get(i).getCircunferencia(), listado.get(i).getLargo());

            volumen+= result;

        }

        return volumen;
    }

    public double getVolumenTotal() {

        ArrayList<Troza> listado = recuperarListaTrozas(1);

        double volumen = 0.0;

        for (int i = 0; i < listado.size(); i++) {

            volumen+= TrozaCalculation.getSmalian(listado.get(i).getCircunferencia(), listado.get(i).getLargo());
        }

        return volumen;
    }

    public ArrayList<String> getFechas(){

        ArrayList<String> fechas = new ArrayList<>();

        String[] columnas = {Constants.COLUMN_VIAJE_FECHA_INGRESO};

        db = helper.getWritableDatabase();
        Cursor cursor = db.query(Constants.TABLE_VIAJES, columnas, null, null, Constants.COLUMN_VIAJE_FECHA_INGRESO, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            fechas.add(cursor.getString(0));
            cursor.moveToNext();
        }

        return fechas;
    }

    public ArrayList<Viaje> getViajesxDia(String fecha){

        ArrayList<Viaje> listado = new ArrayList<>();
        String[] columnas;

        String whereClause = Constants.COLUMN_VIAJE_FECHA_INGRESO + "=?";
        String[] whereArgs = {fecha};

        columnas = getHeaders(Constants.TABLE_VIAJES);

        db = helper.getWritableDatabase();
        Cursor cursor = db.query(Constants.TABLE_VIAJES, columnas, whereClause, whereArgs, null, null, null);

        Viaje viaje;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            viaje = cursorToViaje(cursor);
            listado.add(viaje);
            cursor.moveToNext();
        }

        cursor.close();

        return listado;
    }

    public double getVolumenesxDia(String fecha) {

        ArrayList<Viaje> listado = getViajesxDia(fecha);

        Log.d("Flag", "Viajes de fecha " + fecha + " = " + listado.toString());

        double volumen = 0.0;

        for (Viaje data : listado) {

            volumen+= getVolumenTotalxViaje(data.getViajeid());

        }

        return volumen;
    }

}

