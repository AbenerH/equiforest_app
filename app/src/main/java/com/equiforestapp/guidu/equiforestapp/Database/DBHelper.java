package com.equiforestapp.guidu.equiforestapp.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.VERSION_DB);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_VIAJE);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_TROZAS);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_RODALES);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_UBICACIONES);
            sqLiteDatabase.execSQL(Constants.CREATE_VIEW_TROZAS_VIAJES);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_FACTURACONTENEDOR);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_FACTURADETALLECONTENEDOR);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_PACKINGLIST);
            sqLiteDatabase.execSQL(Constants.CREATE_VIEW_TROZAS_COMERCIALIZACION);
            Log.d("DATABASE", "Base de datos creada");
        }catch ( SQLException e){
            e.printStackTrace();
            Log.e("DATABASE", "Error al crear la base de datos + " + e.getMessage());

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        try{
            sqLiteDatabase.execSQL(Constants.DROP_TABLE_VIAJES);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_VIAJE);

            sqLiteDatabase.execSQL(Constants.DROP_TABLE_TROZAS);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_TROZAS);

            sqLiteDatabase.execSQL(Constants.DROP_TABLE_RODALES);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_RODALES);

            sqLiteDatabase.execSQL(Constants.DROP_TABLE_UBICACIONES);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_UBICACIONES);

            sqLiteDatabase.execSQL(Constants.DROP_VIEW_TROZAS_VIAJES);
            sqLiteDatabase.execSQL(Constants.CREATE_VIEW_TROZAS_VIAJES);

            sqLiteDatabase.execSQL(Constants.DROP_TABLE_FACTURACONTENEDOR);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_FACTURACONTENEDOR);

            sqLiteDatabase.execSQL(Constants.DROP_TABLE_FACTURADETALLECONTENEDOR);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_FACTURADETALLECONTENEDOR);

            sqLiteDatabase.execSQL(Constants.DROP_TABLE_PACKINGLIST);
            sqLiteDatabase.execSQL(Constants.CREATE_TABLE_PACKINGLIST);

            sqLiteDatabase.execSQL(Constants.DROP_VIEW_TROZAS_COMERCIALIZACION);
            sqLiteDatabase.execSQL(Constants.CREATE_VIEW_TROZAS_COMERCIALIZACION);

            Log.d("DATABASE", "Base de datos renovada");

        }catch (SQLException e){
            e.printStackTrace();
            Log.e("DATABASE", "Error al actualizar base de datos" + e.getMessage());

        }
    }


}
