package com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_FContenedorDetalle_Helper;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class DC_TablaDetalle extends AppCompatActivity implements ITableActivity {

    TableView<String[]> tableView;
    Table_FContenedorDetalle_Helper helper;

    String packingId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dc__tabla_detalle);

        packingId = getIntent().getStringExtra("packingId");

        showData();

    }

    @Override
    public void showData() {

        helper = new Table_FContenedorDetalle_Helper(this);
        tableView = (TableView<String[]>)findViewById(R.id.tableView_DetalleContenedores);
        tableView.setColumnCount(5);
        tableView.setHeaderBackgroundColor(Color.parseColor("#3d3d3d"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, helper.getHeaders(Headers.SIMPLE_HEADER)));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, helper.getDetalleContenedor(packingId)));

    }
}
