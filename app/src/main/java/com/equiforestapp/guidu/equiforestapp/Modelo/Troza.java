package com.equiforestapp.guidu.equiforestapp.Modelo;

public class Troza{

    private int id;
    private String clasificacion;
    private double circunferencia;
    private double largo;
    private String viajeid;
    private int serial;

    public Troza() {
    }

    public Troza(String clasificacion, double circunferencia, double largo, int serial) {
        this.clasificacion = clasificacion;
        this.circunferencia = circunferencia;
        this.largo = largo;
        this.serial = serial;
    }

    //Getters & Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public double getCircunferencia() {
        return circunferencia;
    }

    public void setCircunferencia(double circunferencia) {
        this.circunferencia = circunferencia;
    }

    public double getLargo() {
        return largo;
    }

    public void setLargo(double largo) {
        this.largo = largo;
    }

    public String getViajeid() {
        return viajeid;
    }

    public void setViajeid(String viajeid) {
        this.viajeid = viajeid;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    @Override
    public String toString() {
        return "Troza{" +
                "id=" + id +
                ", clasificacion='" + clasificacion + '\'' +
                ", circunferencia=" + circunferencia +
                ", largo=" + largo +
                ", viajeid='" + viajeid + '\'' +
                ", serial=" + serial +
                '}';
    }

}
