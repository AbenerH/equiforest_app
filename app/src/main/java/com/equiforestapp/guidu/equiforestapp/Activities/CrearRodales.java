package com.equiforestapp.guidu.equiforestapp.Activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Rodales_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Rodal;
import com.equiforestapp.guidu.equiforestapp.R;

import java.util.ArrayList;
import java.util.Arrays;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataLongClickListener;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class CrearRodales extends AppCompatActivity implements ITableActivity, AdapterView.OnItemSelectedListener {

    String[] nucleos;

    Spinner clase_spinner;

    //Rodal Mask (Like an ID Card one)
    EditText primero, segundo, tercero;

    TextView nucleo;

    EditText finca;

    Button guardar;

    private TableView<String[]> tableView;
    private Table_Rodales_Helper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_rodales);

        initialize();
        showData();

    }

    private void initialize() {

        nucleos = getResources().getStringArray(R.array.nucleo_array);

        clase_spinner = findViewById(R.id.rodales_clase_spinner);
        primero = findViewById(R.id.rodales_primer_edit);
        segundo = findViewById(R.id.rodales_segundo_edit);
        tercero = findViewById(R.id.rodales_tercer_edit);

        nucleo = findViewById(R.id.rodales_nucleo_text);
        finca = findViewById(R.id.rodales_finca_edit);

        guardar = findViewById(R.id.rodales_guardar_button);

        ArrayAdapter<CharSequence> claseAdapter = ArrayAdapter.createFromResource(this, R.array.clasif_rodales_array, R.layout.support_simple_spinner_dropdown_item);
        claseAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        clase_spinner.setAdapter(claseAdapter);

        clase_spinner.setOnItemSelectedListener(this);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!checkFields()) {

                    StringBuilder nombreRodal = new StringBuilder();
                    Rodal rodal = new Rodal();

                    nombreRodal.append(clase_spinner.getSelectedItem().toString());
                    nombreRodal.append("-");
                    nombreRodal.append(primero.getText().toString());
                    nombreRodal.append("-");
                    nombreRodal.append(segundo.getText().toString());
                    nombreRodal.append("-");
                    nombreRodal.append(tercero.getText().toString());

                    rodal.setRodal(nombreRodal.toString());
                    rodal.setNucleo(nucleo.getText().toString());
                    rodal.setFinca(finca.getText().toString());

                    Log.d("Debugging", rodal.toString());

                    if (new DBAdapter(CrearRodales.this).guardarRodal(rodal)) {

                        Toast.makeText(CrearRodales.this, "Datos insertados correctamente", Toast.LENGTH_SHORT).show();
                        cleanFields();
                        refreshTable();

                    } else {
                        Toast.makeText(CrearRodales.this, "ERROR: No se pudo crear el rodal", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(CrearRodales.this, "No puede haber ningun campo vacio", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void refreshTable() {
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getRodales()));
    }

    private void cleanFields() {

        finca.setText("");
        primero.setText("");
        segundo.setText("");
        tercero.setText("");

    }

    private boolean checkFields() {

        return primero.getText().toString().equals("")
                || segundo.getText().toString().equals("")
                || tercero.getText().toString().equals("")
                || finca.getText().toString().equals("");

    }

    @Override
    public void showData() {

        tableHelper = new Table_Rodales_Helper(this);
        tableView = (TableView<String[]>) findViewById(R.id.rodales_tableView);
        tableView.setColumnCount(3);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders()));

        tableView.addDataLongClickListener(new longClickListenerHandler());
        refreshTable();

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        nucleo.setText(nucleos[i]);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class longClickListenerHandler implements TableDataLongClickListener<String[]> {


        @Override
        public boolean onDataLongClicked(int rowIndex, String[] clickedData) {
            final int id =  Integer.parseInt(((String[]) clickedData)[3]);

            AlertDialog.Builder alerta = new AlertDialog.Builder(CrearRodales.this);
            alerta.setMessage("¿Desea borrar este registro?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            if (new DBAdapter(CrearRodales.this).eliminarRodal(id)) {

                                Toast.makeText(CrearRodales.this, "Registro Borrado", Toast.LENGTH_SHORT).show();
                                refreshTable();

                            } else {
                                Toast.makeText(CrearRodales.this, "El registro no se pudo borrar", Toast.LENGTH_SHORT).show();
                            }

                            dialogInterface.dismiss();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Borrar Rodal");
            myAlert.show();

            return true;
        }

    }
}
