package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.Rodal;

import java.util.ArrayList;

public class Table_Rodales_Helper {

    Context context;

    private String [] headers={"Rodal","Nucleo", "Finca"};
    private String[][] data;

    public Table_Rodales_Helper(Context context) {
        this.context = context;
    }

    public String[] getHeaders() { return headers; }

    public String[][] getRodales(){

        ArrayList<Rodal> listadoRodal= new DBAdapter(context).recuperarListaRodales();
        Rodal rodal;

        data = new String[listadoRodal.size()][4];

        for (int i = 0; i < listadoRodal.size() ; i++) {

            rodal = listadoRodal.get(i);

            data[i][0] = rodal.getRodal();
            data[i][1] = rodal.getNucleo();
            data[i][2] = rodal.getFinca();
            data[i][3] = String.valueOf(rodal.getId());

        }

        return data;

    }
}
