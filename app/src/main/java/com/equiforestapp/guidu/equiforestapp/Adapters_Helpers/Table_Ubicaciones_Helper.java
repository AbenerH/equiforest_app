package com.equiforestapp.guidu.equiforestapp.Adapters_Helpers;

import android.content.Context;

import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Modelo.Ubicaciones;

import java.util.ArrayList;

public class Table_Ubicaciones_Helper {

    Context context;

    private String [] headers={"ID", "Nombre"};
    private String[][] data;

    public Table_Ubicaciones_Helper(Context context) {
        this.context = context;
    }

    public String[] getHeaders() { return headers; }

    public String[][] getUbicaciones(){

        ArrayList<Ubicaciones> listadoUbicaciones= new DBAdapter(context).recuperarListaUbicaciones();
        Ubicaciones ubicaciones;

        data = new String[listadoUbicaciones.size()][2];

        for (int i = 0; i < listadoUbicaciones.size() ; i++) {

            ubicaciones = listadoUbicaciones.get(i);

            data[i][0] = String.valueOf(ubicaciones.getId());
            data[i][1] = ubicaciones.getNombre();
        }

        return data;

    }

}
