package com.equiforestapp.guidu.equiforestapp.Activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Viajes_Helper;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class VolumenesDiarios extends AppCompatActivity {

    TableView<String[]> tableView;
    Table_Viajes_Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volumenes_diarios);

        showData();

    }

    private void showData() {

        helper = new Table_Viajes_Helper(this);
        tableView = findViewById(R.id.tableView_Volumenes);
        tableView.setColumnCount(3);
        tableView.setHeaderBackgroundColor(Color.parseColor("#a6a6a6"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, helper.getHeaders(Headers.DETAIL_HEADER)));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, helper.getViajesxFecha()));
    }
}
