package com.equiforestapp.guidu.equiforestapp.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle.Viajes_TablaDetalle;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_Viajes_Helper;
import com.equiforestapp.guidu.equiforestapp.Database.DBAdapter;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.Modelo.Rodal;
import com.equiforestapp.guidu.equiforestapp.Modelo.Ubicaciones;
import com.equiforestapp.guidu.equiforestapp.Modelo.Viaje;
import com.equiforestapp.guidu.equiforestapp.R;

import java.util.ArrayList;
import java.util.Calendar;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class CrearViaje extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, ITableActivity, AdapterView.OnItemSelectedListener {

    private static Boolean setted = false;
    private static int Id = 0;
    private static String final_date = "";

    private ArrayList<String> nucleos;
    private ArrayList<String> fincas;
    private ArrayList<String> rodales;
    private ArrayList<String> acopios;

    private TextView fecha;
    private EditText nViajes;
    private EditText trozasC;

    private Button guardar, borrar;

    private Spinner nucleo_spinner;
    private Spinner finca_spinner;
    private Spinner rodal_spinner;
    private Spinner acopio_spinner;

    private TableView<String[]> tableView;
    private Table_Viajes_Helper tableHelper;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setted = false;
        Id = 0;

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_agregar_viaje);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_agregar_viaje);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initialize();
        showData();

        changeState();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("Id", Id);
        outState.putBoolean("setted", setted);

/*      outState.putInt("nucleo", getArrayValue(nucleos, nucleo_spinner.getSelectedItem().toString()));
        outState.putInt("finca", getArrayValue(fincas, finca_spinner.getSelectedItem().toString())); */
        outState.putInt("rodal", getArrayValue(rodales, rodal_spinner.getSelectedItem().toString()));
        outState.putString("fecha", fecha.getText().toString());

        if (acopio_spinner.getSelectedItem() != null){
            outState.putInt("patio", getArrayValue(acopios, acopio_spinner.getSelectedItem().toString()));
        }

        outState.putString("viajes", nViajes.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        nucleo_spinner.setSelection(savedInstanceState.getInt("nucleo"));
        finca_spinner.setSelection(savedInstanceState.getInt("finca"));
        rodal_spinner.setSelection(savedInstanceState.getInt("rodal"));
        fecha.setText(savedInstanceState.getString("fecha"));
        acopio_spinner.setSelection(savedInstanceState.getInt("patio"));
        nViajes.setText(savedInstanceState.getString("viajes"));

        Id = savedInstanceState.getInt("Id");
        setted = savedInstanceState.getBoolean("setted");

    }

    @Override
    public void showData() {
        tableHelper = new Table_Viajes_Helper(this);
        tableView = findViewById(R.id.Viajes_tableView);
        tableView.setColumnCount(4);
        tableView.setHeaderBackgroundColor(Color.parseColor("#777777"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, tableHelper.getHeaders(Headers.SIMPLE_HEADER)));
        refreshTable();

        tableView.addDataClickListener((rowIndex, clickedData) -> {

            Id = Integer.parseInt(clickedData[0]);
            rodal_spinner.setSelection(getArrayValue(rodales, clickedData[3]));
            fecha.setText(clickedData[4]);
            acopio_spinner.setSelection(getArrayValue(acopios, clickedData[5]));
            nViajes.setText(clickedData[7]);
            trozasC.setText(clickedData[6]);

            final_date = getFormatFecha(fecha.getText().toString());

            setted = true;
            changeState();

        });

    }

    private String getFormatFecha(String date) {

        return date.replaceAll("[^a-zA-Z0-9 -]", "");
    }

    private void initialize() {

        getRodalesData();

        finca_spinner = findViewById(R.id.fincas_spinner);
        nucleo_spinner = findViewById(R.id.nucleo_spinner);
        rodal_spinner = findViewById(R.id.rodal_spinner);
        fecha = findViewById(R.id.tv_fecha);
        acopio_spinner = findViewById(R.id.acopio_spinner);
        nViajes = findViewById(R.id.nviaje_edit);
        trozasC = findViewById(R.id.trozasC_edit);
        Button limpiar = findViewById(R.id.limpiarViajes_button);
        guardar = findViewById(R.id.guardarViaje_button);
        borrar = findViewById(R.id.eliminarViaje_button);
        //registrar = (Button) findViewById(R.id.registrarTroza_button);
        Button verTabla = findViewById(R.id.verTablaV_button);

        final ArrayAdapter<String> nucleoAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, nucleos);
        nucleo_spinner.setAdapter(nucleoAdapter);

        final ArrayAdapter<String> fincaAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, fincas);
        finca_spinner.setAdapter(fincaAdapter);

        final ArrayAdapter<String> rodalAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, rodales);
        rodal_spinner.setAdapter(rodalAdapter);

        getAcopioData();

        /*final ArrayAdapter<String> acopioAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, acopios);
        acopio_spinner.setAdapter(acopioAdapter);*/

        nucleo_spinner.setEnabled(false);
        finca_spinner.setEnabled(false);
        rodal_spinner.setOnItemSelectedListener(this);

        fecha.setOnClickListener(view -> showDatePickerDialog());


        guardar.setOnClickListener(view -> {

            if(!checkFields()){

                Viaje viaje = new Viaje();

                viaje.setNucleo(nucleo_spinner.getSelectedItem().toString());
                viaje.setFinca(finca_spinner.getSelectedItem().toString());
                viaje.setRodal(rodal_spinner.getSelectedItem().toString());
                viaje.setFechaIngreso(fecha.getText().toString());
                viaje.setUbicacion(acopio_spinner.getSelectedItem().toString());
                viaje.setN_viaje(Integer.parseInt(nViajes.getText().toString()));
                viaje.setNoTrozasC(Integer.parseInt(trozasC.getText().toString()));

                viaje.setViajeid(generarViajeId(viaje.getRodal(), viaje.getN_viaje(), final_date));

                Log.d("Debugging Viajes", viaje.toString());

                if (setted) {

                    Intent intent = new Intent(CrearViaje.this, Registro_Trozas_Cosechas.class);
                    intent.putExtra("finca", viaje.getFinca());
                    intent.putExtra("rodal", viaje.getRodal());
                    intent.putExtra("nViaje", viaje.getN_viaje());
                    intent.putExtra("viajeId", viaje.getViajeid());

                    startActivity(intent);

                } else {

                    if (new DBAdapter(CrearViaje.this).getViajeId(viaje.getViajeid())){

                        Toast.makeText(CrearViaje.this, "Ya existe un registro con este identificador", Toast.LENGTH_SHORT).show();

                    }

                    else if (new DBAdapter(CrearViaje.this).guardarViaje(viaje)){

                        Toast.makeText(CrearViaje.this, "Datos insertados correctamente", Toast.LENGTH_SHORT).show();
                        cleanfields();
                        refreshTable();
                    } else {
                        Toast.makeText(CrearViaje.this, "Error al guardar", Toast.LENGTH_SHORT).show();
                    }
                }

            }else{

                Toast.makeText(CrearViaje.this, "No pueden haber datos vacios", Toast.LENGTH_SHORT).show();

            }

        });

        verTabla.setOnClickListener(view -> {
            Intent intent = new Intent(CrearViaje.this, Viajes_TablaDetalle.class);
            startActivity(intent);
        });

        limpiar.setOnClickListener(view -> {
            cleanfields();
            setted = false;
            changeState();
        });

        limpiar.setOnLongClickListener(view -> {
            cleanAllfields();
            setted = false;
            changeState();
            return true;
        });

        /*registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String viajeId = rodal_spinner.getSelectedItem().toString() + Integer.parseInt(nViajes.getText().toString()) + fecha.getText().toString();

                Intent intent = new Intent(CrearViaje.this, Registro_Trozas_Cosechas.class);
//                intent.putExtra("id", Id);
                intent.putExtra("finca", finca_spinner.getSelectedItem().toString());
                intent.putExtra("rodal", rodal_spinner.getSelectedItem().toString());
                intent.putExtra("nViaje", Integer.parseInt(nViajes.getText().toString()));
                intent.putExtra("viajeId", viajeId);

                startActivity(intent);
            }
        });*/

        borrar.setOnClickListener(view -> {

            AlertDialog.Builder alerta = new AlertDialog.Builder(CrearViaje.this);
            alerta.setMessage("¿Desea borrar este viaje?")
                    .setCancelable(false)
                    .setPositiveButton("Si", (dialogInterface, i) -> {

                        setted = false;

                        String viajeId = generarViajeId(rodal_spinner.getSelectedItem().toString(), Integer.parseInt(nViajes.getText().toString()), final_date);

                        Log.d("Debugging", "ViajeID: " + viajeId);

                        if (new DBAdapter(CrearViaje.this).eliminarViaje(viajeId)){
                            Toast.makeText(CrearViaje.this, "Registro eliminado exitosamente", Toast.LENGTH_SHORT).show();
                            cleanAllfields();
                            changeState();
                            refreshTable();

                        }else{
                            Toast.makeText(CrearViaje.this, "Error al eliminar", Toast.LENGTH_SHORT).show();
                        }

                        dialogInterface.dismiss();
                    })
                    .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

            AlertDialog myAlert = alerta.create();
            myAlert.setTitle("Borrar Viaje");
            myAlert.show();
        });

    }

    private void getRodalesData() {

        nucleos = new ArrayList<>();
        fincas = new ArrayList<>();
        rodales = new ArrayList<>();

        ArrayList<Rodal> listado = new DBAdapter(CrearViaje.this).recuperarListaRodales();

        for (int i = 0; i < listado.size(); i++) {

            nucleos.add(listado.get(i).getNucleo());
            fincas.add(listado.get(i).getFinca());
            rodales.add(listado.get(i).getRodal());
        }

    }

    private void getAcopioData(){

        acopios = new ArrayList<>();

        ArrayList<Ubicaciones> ubicaciones = new DBAdapter(CrearViaje.this).recuperarListaUbicaciones();

        for (int i = 0; i < ubicaciones.size() ; i++) {

            acopios.add(ubicaciones.get(i).getNombre());

        }

        final ArrayAdapter<String> acopioAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, acopios);
        acopio_spinner.setAdapter(acopioAdapter);

    }

    private int getArrayValue(ArrayList<String> array, String toFind) {
        int position = 0;

        for (int i = 0; i < array.size(); i++) {

            if (array.get(i).equals(toFind)) {

                position = i;
            }
        }

        return position;
    }

    private String generarViajeId(String rodal, int numeroViaje, String date){

        StringBuilder viajeId = new StringBuilder();
        String nViaje;

        nViaje = "000" + numeroViaje;
        viajeId.append(rodal).append(nViaje.substring(nViaje.length() - 3)).append(date);

        Log.d("Debugging Viajes", "ViajeID Generado: " + viajeId.toString());

        return viajeId.toString();
    }


    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                this,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        month = month + 1;
        String dia = String.valueOf(day);
        String mes = String.valueOf(month);

        if(day < 10){

            dia = "0" + day;
        }

        if (month < 10){

            mes = "0" + month;
        }

        String date = mes + "/" + dia + "/" + year;
        final_date = getFormatFecha(date);

        fecha.setText(date);

    }

    private void refreshTable() {
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, tableHelper.getViajes()));
    }

    private void changeState() {

        if (setted) {

            guardar.setText("Asignar");
            borrar.setEnabled(true);
            rodal_spinner.setEnabled(false);
            fecha.setEnabled(false);
            acopio_spinner.setEnabled(false);
            nViajes.setEnabled(false);
            trozasC.setEnabled(false);
     //       registrar.setEnabled(true);

        } else {

            Id = 0;
            guardar.setText("Guardar");
            rodal_spinner.setEnabled(true);
            fecha.setEnabled(true);
            acopio_spinner.setEnabled(true);
            nViajes.setEnabled(true);
            trozasC.setEnabled(true);
            borrar.setEnabled(false);
//            registrar.setEnabled(false);

        }

    }

    private boolean checkFields(){

        return rodal_spinner.getSelectedItem().toString().equals("Seleccione") || fecha.getText().equals("") || acopio_spinner.getSelectedItem() == null || nViajes.getText().toString().equals("");

    }


    private void cleanfields() {

        nViajes.setText("");
        trozasC.setText("");

    }

    private void cleanAllfields() {

        rodal_spinner.setSelection(0);
        fecha.setText("");
        acopio_spinner.setSelection(0);
        nViajes.setText("");
        trozasC.setText("");

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        finca_spinner.setSelection(i);
        nucleo_spinner.setSelection(i);

//        getAcopioData();


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
