package com.equiforestapp.guidu.equiforestapp.Activities.Vista_Detalle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Headers;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_FContenedorDetalle_Helper;
import com.equiforestapp.guidu.equiforestapp.Adapters_Helpers.Table_PackingList_Helper;
import com.equiforestapp.guidu.equiforestapp.Interfaces.ITableActivity;
import com.equiforestapp.guidu.equiforestapp.R;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class PL_TablaDetalle extends AppCompatActivity implements ITableActivity {

    TableView<String[]> tableView;
    Table_PackingList_Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pl_tabla_detalle);

        showData();
    }

    @Override
    public void showData() {

        helper = new Table_PackingList_Helper(this);
        tableView = (TableView<String[]>)findViewById(R.id.tableView_PackingList);
        TableColumnWeightModel columnModel = new TableColumnWeightModel(7);
        columnModel.setColumnWeight(0, 2);
        columnModel.setColumnWeight(1, 1);
        columnModel.setColumnWeight(2, 2);
        columnModel.setColumnWeight(3, 1);
        columnModel.setColumnWeight(4, 2);
        columnModel.setColumnWeight(5, 2);
        columnModel.setColumnWeight(6, 2);
        tableView.setColumnModel(columnModel);
        tableView.setHeaderBackgroundColor(Color.parseColor("#3d3d3d"));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, helper.getHeaders(Headers.DETAIL_HEADER)));

        tableView.setDataAdapter(new SimpleTableDataAdapter(this, helper.getPackingLists()));

    }
}
